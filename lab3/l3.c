#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

void sighandler(int);

int main (int argc, char* argv[]) {
	struct sigaction act;

	act.sa_handler = &sighandler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_NOCLDWAIT;
	if (sigaction(SIGCHLD, &act, NULL) < 0){
		perror("Error: ");
		return 1;
	}

	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\tChild pid is %d\n", getpid());
		sleep(2);
		printf("\tChild is dead\n");
		exit(0);
	}
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\tChild pid is %d\n", getpid());
		sleep(2);
		printf("\tChild is dead\n");
		exit(0);
	}
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\tChild pid is %d\n", getpid());
		sleep(2);
		printf("\tChild is dead\n");
		exit(0);
	}
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\tChild pid is %d\n", getpid());
		sleep(2);
		printf("\tChild is dead\n");
		exit(0);
	}
	if (pid != 0) {
		printf("Parent pid is %d\n", getpid());
		while (1);
		printf("Parent is dead\n");
	}
	return 0;
}

void sighandler(int signum) {
	struct sigaction act;
	printf("Dead.\n");
	act.sa_handler = sighandler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_NOCLDWAIT;
	if (sigaction(SIGCHLD, &act, NULL) < 0){
		perror("Error: ");
	}
}
