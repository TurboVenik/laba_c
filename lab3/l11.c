#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include <sys/wait.h>

void sighandler(int);

int main(int argc, const char* argv[]) {
  time_t t;
  srand(time(NULL));
  int r1 = rand() % 7;
  int r2 = rand() % 7;

  pid_t waitpid;
  struct sigaction newact;
  pid_t child;
  int i;
  int status;

  child = fork();
  if (child < 0) {
    perror("Can't create fork:");
    return 1;
  } else if (child == 0) {

    if (sigemptyset(&(newact.sa_mask)) < 0) {
      perror("Error empty mask set:");
      return 1;
    }

    newact.sa_handler = &sighandler;

    sigaction(SIGUSR1, &newact, NULL);

    sigset_t newset;
    sigset_t oldset;
    sigemptyset(&newset);
    sigaddset(&newset, SIGUSR1);
    sigprocmask(SIG_BLOCK, &newset, &oldset);

    for (int i = 0; i < 1 +r1; ++i){
			for (int j = 0; j < 10000; ++j){
				for (int k = 0; k < 10000; ++k);
			}
			printf("\ti: %d\n", i);
		}

    sigsuspend(&oldset);
    printf("===== AFTER ALL ====\n");
    printf("Child ended\n");
    return 0;
  } else {

    for (int i = 0; i < 1 + r2; ++i){
      for (int j = 0; j < 10000; ++j){
        for (int k = 0; k < 10000; ++k);
      }
      printf("\t\ti: %d\n", i);
    }
    kill(child, SIGUSR1);
    printf("        =====  SIGUSR1 =====\n");

    if ((waitpid = wait(&status)) < 0) {
      perror("Error wait:");
      return 1;
    }
    printf("Parent ended\n");
  }

  return 0;
}

void sighandler(int sig) {
  printf("=====  Handler  =====\n");
}
