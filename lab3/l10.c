#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

void sighandler(int);

int main (int argc, char* argv[]) {
	struct sigaction setup_action;
	sigset_t block_mask;

	sigemptyset (&block_mask);
	/* Block other terminal-generated signals while handler runs. */
	sigaddset (&block_mask, SIGUSR1);
	sigaddset (&block_mask, SIGUSR2);
	setup_action.sa_handler = sighandler;
	setup_action.sa_mask = block_mask;
	setup_action.sa_flags = 0;
	sigaction (SIGINT, &setup_action, NULL);
	while (1){
		sleep(1);
		printf("\t PID: %d\n", getpid());
	}
	return 0;
}

void sighandler(int signum) {
	printf("Caught signal %d, coming out...\n", signum);
	for (int i = 0; i < 10; ++i){
		for (int j = 0; j < 10000; ++j){
			for (int k = 0; k < 10000; ++k){
			;
			}
		}
		printf("Cycle number is %d\n", i);
	}
}
