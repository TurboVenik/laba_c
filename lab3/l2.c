#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

void sighandler(int);

int main (int argc, char* argv[]) {
	struct sigaction act;

	act.sa_handler = sighandler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	if (sigaction(SIGRTMIN, &act, NULL) < 0){
		perror("Error: ");
		return 1;
	}

	while(1) {
		printf("...\n");
		sleep(1);
	}
	return(0);
}

void sighandler(int signum) {
	printf("Signal %d was there\n", signum);
	sleep(5);
	// struct sigaction act;
	// act.sa_handler = SIG_DFL;
	// if (sigaction(SIGINT, &act, NULL) < 0){
	// 	perror("Error: ");
	// 	return;
	// }
}
