#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

void sighandler(int);

int main (int argc, char* argv[]) {
	if (signal(SIGINT, sighandler) == SIG_ERR) {
		printf("Error in 'signal()'\n");
	}
	sleep(1000);
	while(1) {
		printf("...\n");
		sleep(1);
	}
	return(0);
}

void sighandler(int signum) {
	printf("Signal %d was couched\n", signum);
	if (signal(SIGINT, SIG_DFL) == SIG_ERR) {
		printf("Error in 'signal()'\n");
	}
}
