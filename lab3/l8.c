#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (int argc, char* argv[]) {
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\tChild PID: %d\n", getpid());
		if (signal(SIGINT, SIG_IGN) == SIG_ERR) {
				printf("Error in 'signal()' \n");
		}
		for (int i = 0; i < 10000; ++i){
			for (int j = 0; j < 10000; ++j){
				for (int k = 0; k < 10000; ++k){
				;
				}
			}
			printf("\ti: %d\n", i);
		}
		printf("\tChild was ended\n");
		exit(0);
	}
	else {
		printf("Parent pid is %d\n", getpid());
		sleep(1);
		int res = kill(pid, SIGINT);
		if (res == -1){
			perror("Error: ");
			return -1;
		}
		printf("===== SIGINT =====\n");
		int status;
		int pid;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}
		printf("Child (%d) \nreturned: %d\n", pid, WEXITSTATUS(status));
		if (WIFSIGNALED(status)) {
			printf("Recieved signal: %d\n", WTERMSIG(status));
		}
		else {
			printf("Didn't recieve any signals\n");
			printf("Parent dead\n");
		}
	}
	return 0;
}
