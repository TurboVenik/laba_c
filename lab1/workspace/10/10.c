#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>


int vivod(int code) {
    struct stat buf;
    char* buffer;
    int i;
    int length;
    fstat(code, &buf);
    int size = buf.st_size;
    buffer = malloc(1);
    lseek(code,-1,SEEK_END);
    for (i=0; i < size; i++) {
        read(code, buffer, 1);
        printf("%s", buffer);
        lseek(code,-2,SEEK_CUR);
    }
    free(buffer);
}

int main(int argc, char **argv)
{
    int code;
    code = open(argv[1],  O_RDWR);
    printf("1. OPEM new file code is %d: %s\n", code, argv[1]);
    if (code == -1) {
        printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n", errno, sys_errlist[errno]);
        perror(" PError");
    }
    vivod(code);
    close(code);
    return 0;
}
