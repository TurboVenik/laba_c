
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    int max = -1;
    struct stat buf;
    for (int i = 1; i<argc; i++) {
        int code;
        code = open(argv[i],  O_RDWR);
        printf("1. OPEM new file code is %d: %s\n", code, argv[1]);
        if (code == -1) {
            printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n", errno, sys_errlist[errno]);
            perror(" PError");
        }
        fstat(code, &buf);
        if (max < buf.st_size)
            max = buf.st_size;
        close(code);
    }
    printf("Max file size in arguments is %d bytes", max);
    return 0;
}

