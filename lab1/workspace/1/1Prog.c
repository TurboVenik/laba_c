#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

int main(int argc, char **argv)
{	
	int i;
	int code;
	int mask = 0;	
	for (i=2; i<argc; i++) {
		switch (atoi(argv[i])) {
			case 1:
				mask = mask | O_RDONLY;
				break;
			case 2:
				mask = mask | O_WRONLY;
				break;
			case 3: 
				mask = mask | O_RDWR;
				break;
			default:
				break;
			}
	}
	code = open(argv[1], mask); 	
	printf("1. Code for open file is %d: %s\n", code, argv[1]);
	if (code == -1) {
		printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
			, errno, sys_errlist[errno]);
			perror(" PError");
	}
	return 0;
}

