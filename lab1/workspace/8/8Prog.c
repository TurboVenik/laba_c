#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int copy(int code, int code2) {
    struct stat buf;
    char* buffer;
    int i;

    int length;
    fstat(code, &buf);
    int size = buf.st_size;
    buffer = malloc(1);
    printf("%d\n", size);
    int hall = 0;
    for (i=0; i < size; i++) {
        read(code, buffer, 1);
        if (buffer[0] != 0) {
            lseek(code2,hall,SEEK_CUR);
            hall =0;
            write(code2, buffer, 1);
        } else {
            hall++;
        }
    }
    lseek(code2,hall,SEEK_CUR);
    printf("%s\n", buffer);
    free(buffer);
}

int main(int argc, char **argv)
{
    int code;
    int code2;
    int mask = 0;
    int bufferIter = 0;

    code = open(argv[1],  O_RDWR);
    code2 = open(argv[2],  O_RDWR |O_CREAT|O_TRUNC,S_IRWXU);
    printf("1. Creating new file code is %d: %s\n", code, argv[1]);
    if (code == -1) {
        printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
                , errno, sys_errlist[errno]);
        perror(" PError");
    }
    code2 = creat(argv[2], S_IRUSR|S_IWUSR);
    printf("1. Creating new file code is %d: %s\n", code, argv[2]);
    if (code == -1) {
        printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
                , errno, sys_errlist[errno]);
        perror(" PError");
    }
    copy(code,code2);
    close(code);
    close(code2);
    return 0;
}