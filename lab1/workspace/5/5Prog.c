#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int main(int argc, char **argv)
{
    int i;
    int code;
    int code2;
    int mask = 0;
    char* buffer;
    int bufferIter = 0;


    code = open(argv[1],  O_RDWR |O_CREAT|O_TRUNC,S_IRWXU);
    code2 = open(argv[2],  O_RDWR |O_CREAT|O_TRUNC,S_IRWXU);
    printf("1. Creating new file code is %d: %s\n", code, argv[1]);
    if (code == -1) {
        printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
                , errno, sys_errlist[errno]);
        perror(" PError");
    }
    code2 = creat(argv[2], S_IRUSR|S_IWUSR);
    printf("1. Creating new file code is %d: %s\n", code, argv[2]);
    if (code == -1) {
        printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
                , errno, sys_errlist[errno]);
        perror(" PError");
    }
    int length;
    char text[100] = "SOME TEXT HERE";
    length = strlen(text);
    lseek(code, 0, SEEK_SET);
    write(code, text, length);
    write(code, "1\n", 2);
    lseek(code, 0 , SEEK_END);
    write(code, text, length);
    write(code, "2\n", 2);

    length = strlen(text);
    lseek(code2, 0, SEEK_SET);
    write(code2, text, length);
    write(code2, "1\n", 2);
    lseek(code2, 100000 , SEEK_END);
    write(code2, text, length);
    write(code2, "2\n", 2);

    close(code);
    free(buffer);
    return 0;
}