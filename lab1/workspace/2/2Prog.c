#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int main(int argc, char **argv)
{	
	int i;
	int code;
	int mask = 0;
	char* buffer;
	int bufferIter = 0;
	int isDifferentPosition = 0;
	i = 2;
	if (atoi(argv[2]) == 0) {
		isDifferentPosition = 1;
		i++;
	}
	for (i; i<argc; i++) {
		switch (atoi(argv[i])) {
			case 1:
				mask = mask | S_IRWXU;
				break;
			case 2:
				mask = mask | S_IRUSR;
				break;
			case 3: 
				mask = mask | S_IWUSR;
				break;
			case 4: 
				mask = mask | S_IXUSR;
				break;
			case 5: 
				mask = mask | S_IRWXG;
				break;	
			case 6:
				mask = mask | S_IRGRP;
				break;
			case 7: 
				mask = mask | S_IWGRP;
				break;
			case 8: 
				mask = mask | S_IXGRP;
				break;
			case 9: 
				mask = mask | S_IRWXO;
				break;	
			case 10:
				mask = mask | S_IROTH;
				break;
			case 11: 
				mask = mask | S_IWOTH;
				break;
			case 12: 
				mask = mask | S_IXOTH;
				break;		
			default:
				break;
			}
	}
	code = creat(argv[1], mask); 	
	printf("1. Creating new file code is %d: %s\n", code, argv[1]);
	if (code == -1) {
		printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
			, errno, sys_errlist[errno]);
			perror(" PError");
	}
	int length;
	char text[100] = "SOME TEXT HERE";
	length = strlen(text);
	write(code, text, length);
	write(code,"1\n",2);
	close(code);
	buffer = malloc(length+1);
	for (bufferIter =0; bufferIter<length; bufferIter++) {
		buffer[bufferIter] = '\0';
		}
	code = open(argv[1], O_RDONLY);
	printf("2. Opening new file code is %d: %s\n", code, argv[1]);
	if (code == -1) {
		printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
			, errno, sys_errlist[errno]);
			perror(" PError");
	}
	read(code, buffer, length+1);
	close(code);
	printf("Result of reading with R\n",buffer);
	printf("%s\n",buffer);
	code = open(argv[1], O_RDWR);
	printf("3. Opening new file code is %d: %s\n", code, argv[1]);
	if (code == -1) {
		printf(" ERROR!\n Code is %d\n The text of Sys Error List is: %s\n"
			, errno, sys_errlist[errno]);
			perror(" PError");
	}
	read(code, buffer, length+1);
	printf("Result of reading with RW:\n");
	printf("%s\n",buffer);
	if (isDifferentPosition == 1) {
		lseek(code, 0, 1);
		write(code, text, length);
		write(code, "2\n", 2);
		lseek(code, 0 , 3);
		write(code, text, length);
		write(code, "3\n", 2);
	}
	close(code);
	free(buffer);
	return 0;
}

