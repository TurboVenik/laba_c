#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>


#define MAX_STR_LEN 80

int printMenu();

int openFile(char *);
int createFile(char *, int);
int readFromFile(char *);
int openFileForReadAndWrite(char *, int);
int createSpareFile(char *, int);

int main (int argc, char *argv[])
{

	

	int fd;
	char c;
	struct stat buf;
	
	fd = open(argv[1], O_RDONLY);
	
	fstat(fd, &buf);
	
	printf("\n");
	
	lseek(fd, sizeof(char), SEEK_END);
	
	for(int i = buf.st_size; i >= 0; i--)
	{
		read(fd, &c, sizeof(char));
		printf("%c", c);
		lseek(fd, -2 * sizeof(char), SEEK_CUR);
	}

	return 0;
}
