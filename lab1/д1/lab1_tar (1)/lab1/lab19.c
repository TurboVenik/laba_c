#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>


#define MAX_STR_LEN 80

int printMenu();

int openFile(char *);
int createFile(char *, int);
int readFromFile(char *);
int openFileForReadAndWrite(char *, int);
int createSpareFile(char *, int);

int main (int argc, char *argv[])
{
	char fileName1[80], fileName2[80];

	printf("\nargc = %d", argc);

	if (argc == 1)
	{
		printf("\nEnter 1 file: ");
		scanf("%s", fileName1);
		printf("\nEnter 2 file: ");
		scanf("%s", fileName2);
	}
	
	if (argc == 3)
	{
		strcpy(fileName1, argv[1]);
		strcpy(fileName2, argv[2]);
	}
	
	
	if (argc == 2)
	{
		printf("\nEnter 2 file: ");
		scanf("%s", fileName2);
	}
	


	int fd1, fd2;
	
	
    fd1 = open(fileName1, O_RDONLY);
    fd2 = open(fileName2, O_WRONLY | O_CREAT | O_TRUNC, 777);
	
	dup2(fd1, 1);
	dup2(fd2, 1);
	
	char c;
	struct stat buf;
	
	
	fstat(fd1, &buf);
	
	for(int i = 0; i < buf.st_size; i++)
	{
		read(fd1, &c, sizeof(char));
		write(fd2, &c, sizeof(char));
	}
	
	close(fd1);
	close(fd2);

	return 0;
}
