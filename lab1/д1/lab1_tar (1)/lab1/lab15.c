#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>


#define MAX_STR_LEN 80

int printMenu();

int openFile(char *);
int createFile(char *, int);
int readFromFile(char *);
int openFileForReadAndWrite(char *, int);
int createSpareFile(char *, int);

int main (int argc, char *argv[])
{

	int key;
	char *fileName;
	int rules;

	fileName = argv[1];
	rules = strtoul(argv[2], NULL, 8);

	while(key = printMenu())
	{
		switch(key)
		{
			case 1: openFile(fileName);
				break;
			case 2: createFile(fileName, rules);
				break;
			case 3: readFromFile(fileName);
				break;
			case 4: openFileForReadAndWrite(fileName, rules);
				break;
			case 5: createSpareFile(fileName, rules);
				break;
			default: 
				break;
		}
	}

	return 0;
}

int printMenu()
{
	int key;

	printf("\n[1] Open file");
	printf("\n[2] Create file");
	printf("\n[3] Read from file");
	printf("\n[4] State for read and open file");
	printf("\n[5] Create sparse file");
	printf("\n[0] Exit");
	printf("\nEnter menu item: ");
	scanf("%d", &key);

	return key;
}

int openFile(char *fileName)
{
	int fd;
	
	printf("\n%s", fileName);

	fd = open(fileName, O_RDONLY);

	printf("\nerrno = %d", errno);

	printf("\nsyserrlist = %s", sys_errlist[errno]);

	perror("perror =");

	printf("\n");

	return close(fd);
}

int createFile(char *fileName, int rules)
{
	int fd;
	char *someString;
	
	printf("\n%s", fileName);
	
    fd = open(fileName, O_WRONLY | O_CREAT | O_TRUNC, rules);
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "My first test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "\nMy second test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "\nMy third test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	printf("\n");
	
	return close(fd);
}

int readFromFile(char *fileName)
{
	int fd;
	char c;
	struct stat buf;
	
	fd = open(fileName, O_RDONLY);
	
	fstat(fd, &buf);
	
	printf("\n");
	
	for(int i = 0; i < buf.st_size; i++)
	{
		read(fd, &c, sizeof(char));
		printf("%c", c);
	}
	
	printf("\n");
	
	return close(fd);
}

int openFileForReadAndWrite(char *fileName, int rules)
{
	int fd, result;
	
	result = fd = open(fileName, O_RDWR, rules);
	
	printf("\nsyserrlist = %s, File state = %d\n", sys_errlist[errno], result);
	
	return close(fd);
}

int createSpareFile(char *fileName, int rules)
{
	int fd;
	char *someString;
	
	fd = open(fileName, O_RDWR | O_CREAT, rules);
	
	printf("\nsyserrlist = %s, File state = %d\n", sys_errlist[errno], fd);
	
	someString = "My first test string!";
	
	write(fd, someString, strlen(someString));
	
	lseek(fd, sizeof(char) * (rand() % 10), 1);
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "\nMy second test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	printf("\n");
	
	return close(fd);
}








