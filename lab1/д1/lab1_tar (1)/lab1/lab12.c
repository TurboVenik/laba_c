#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>


#define MAX_STR_LEN 80

int printMenu();

int openFile(char *);
int createFile(char *, int);
int readFromFile(char *);
int openFileForReadAndWrite(char *, int);
int createSpareFile(char *, int);

int main (int argc, char *argv[])
{
	char *fileName;
	int rules;

	fileName = argv[1];
	rules = strtoul(argv[2], NULL, 8);

	int fd;
	char *someString;
	
	printf("\n%s", fileName);
	
    fd = creat(fileName, rules);
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "My first test string!666777788888";
	
	int test;
	test = write(fd, someString, strlen(someString));
	printf("\ntest = %d", test);
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "\nMy second test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "\nMy third test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	printf("\n");
	
	close(fd);
	
	char c;
	struct stat buf;
	
	fd = open(fileName, O_RDONLY);
	
	fstat(fd, &buf);
	
	printf("\n");
	
	for(int i = 0; i < buf.st_size; i++)
	{
		read(fd, &c, sizeof(char));
		printf("%c", c);
	}
	
	printf("\n");
	
	return close(fd);

	return 0;
}
