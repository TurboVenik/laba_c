#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
	char *        fileName;
	int fd;//, rules;

	fileName = argv[1];
	//rules = strtoul(argv[2], NULL, 8);
	
	printf("\n%s", fileName);

	fd = open(fileName, O_RDONLY);

	printf("\nerrno = %d", errno);

	printf("\nsyserrlist = %s", strerror(errno));

	perror("perror =");

	printf("\n");

	close(fd);
	
	return 0;
}
