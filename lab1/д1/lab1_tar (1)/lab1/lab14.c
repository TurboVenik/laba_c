#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>


#define MAX_STR_LEN 80

int printMenu();

int openFile(char *);
int createFile(char *, int);
int readFromFile(char *);
int openFileForReadAndWrite(char *, int);
int createSpareFile(char *, int);

int main (int argc, char *argv[])
{
	char *fileName;
	int rules;

	fileName = argv[1];
	rules = strtoul(argv[2], NULL, 8);

	int fd;
	char *someString;
	
	fd = open(fileName, O_RDWR | O_CREAT, rules);
	
	printf("\nsyserrlist = %s, File state = %d\n", sys_errlist[errno], fd);
	
	someString = "My first test string!";
	
	write(fd, someString, strlen(someString));
	
	lseek(fd, sizeof(char) * (rand() % 10), 1);
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	someString = "\nMy second test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	lseek(fd, sizeof(char) * (rand() % 10), 3);
	
	someString = "\nMy second test string!";
	
	write(fd, someString, strlen(someString));
	
	printf("\nsyserrlist = %s, fd = %d", sys_errlist[errno], fd);
	
	printf("\n");
	
	return close(fd);

	return 0;
}
