#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>


#define MAX_STR_LEN 80

int printMenu();

int openFile(char *);
int createFile(char *, int);
int readFromFile(char *);
int openFileForReadAndWrite(char *, int);
int createSpareFile(char *, int);

int main (int argc, char *argv[])
{

	int fd;
	struct stat buf;
	char * fileName;

	int max;
	max = 0;

	for (int i = 0; i < argc; i++)
	{
		fd = open(argv[i + 1], O_RDONLY);
	
		fstat(fd, &buf);
	
		close(fd);
		
		if (max < buf.st_size)
		{
			max = buf.st_size;
			fileName = argv[i + 1];
		}
	}

	printf("File: %s, Size: %d", fileName, max);

	return 0;
}
