

	
#include <stdio.h>
#include <sys/io.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>


#define MAX_STR_LEN 80


int main (int argc, char *argv[])
{
	char *fileName;
	int fd;

	fileName = argv[1];
	
	struct stat buf;
	
	fd = open(fileName, O_RDONLY);
	
	fstat(fd, &buf);
	
	printf("File Size: \t\t%ld bytes\n",buf.st_size);
    printf("Number of Links: \t%ld\n",buf.st_nlink);
    printf("File inode: \t\t%ld\n",buf.st_ino);
	
	close(fd);

	return 0;
}
