#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>


int copy() {
    struct stat buf;
    char* buffer;
    int i;

    int length;
    fstat(0, &buf);
    int size = buf.st_size;
    buffer = malloc(1);
    int hall = 0;
    for (i=0; i < size; i++) {
        read(0, buffer, 1);
        if (buffer[0] != 0) {
            lseek(1,hall,SEEK_CUR);
            hall =0;
            write(1, buffer, 1);
        } else {
            hall++;
        }
    }
    lseek(1,hall,SEEK_CUR);
    printf("%s\n", buffer);
    free(buffer);
}

int main(int argc, char **argv)
{
    int fd;
    int fd2;
    int mask = 0;
    int bufferIter = 0;

    fd = open(argv[1],  O_RDWR);
    fd2 = open(argv[2],  O_RDWR |O_CREAT|O_TRUNC,S_IRWXU);
    printf("1. Creating new file fd is %d: %s\n", fd, argv[1]);
    if (fd == -1) {
        printf(" ERROR!\n fd is %d\n The text of Sys Error List is: %s\n"
                , errno, sys_errlist[errno]);
        perror(" PError");
    }
    fd2 = creat(argv[2], S_IRUSR|S_IWUSR);
    printf("1. Creating new file fd is %d: %s\n", fd, argv[2]);
    if (fd == -1) {
        printf(" ERROR!\n fd is %d\n The text of Sys Error List is: %s\n"
                , errno, sys_errlist[errno]);
        perror(" PError");
    }
    dup2(fd,0);
    dup2(fd2,1);
    copy();
    close(fd);
    close(fd2);
    return 0;
}
