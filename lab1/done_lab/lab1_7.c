#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>
#include<time.h>

int main(int argc, char *argv[]) {
	struct stat s;
	char *ptype;
	lstat(argv[1], &s);

	if(S_ISREG(s.st_mode)) ptype = "обычный файл";
	else if (S_ISDIR(s.st_mode)) ptype = "каталог";
	else if (S_ISLNK(s.st_mode)) ptype = "симв связь";
	else if (S_ISCHR(s.st_mode)) ptype = "симв устройство";
	else if (S_ISBLK(s.st_mode)) ptype = "блочное устройство";
	else if (S_ISSOCK(s.st_mode)) ptype = "сокет";
	else if (S_ISFIFO(s.st_mode)) ptype = "fifo";
	else ptype = "неизвестный тип";

	//тип
        printf("type = %s\n", ptype);
	//права доступа
        printf("perm = %o\n ", s.st_mode);
	//номер inode
        printf("inode =  %d\n", s.st_ino);
	//число связей
	printf("nlink = %d\n", s.st_nlink);
	//устройство
	printf("dev = (%d,%d)\n", major(s.st_dev), minor(s.st_dev));
        //владелец
	printf("UID = %d\n", s.st_uid);
	printf("GID = %d\n", s.st_gid);
	// номер устройства
	printf("rdev = (%d,%d)\n", major(s.st_rdev), minor(s.st_rdev));
	//размер
	printf("size = %d\n", s.st_size);
	//время  доступа модификации и модификации метаданных
	printf("atime = %s\n", ctime(&s.st_atime));
	printf("mtime = %s\n", ctime(&s.st_mtime));
	printf("ctime = %s\n", ctime(&s.st_ctime));
}
