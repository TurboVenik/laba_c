#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>
int main(int argc, char *argv[]) {
  int fd;

  fd=open(argv[1],  O_RDWR);
	perror("open ");
	printf("open %d\n", fd);

	if (fd < 0) {
    perror("Error step_1 ");
		return 1;
  }

	printf("lseek1 %d\n", lseek(fd, 100000, SEEK_SET));
	perror("lseek1 ");

	printf("write1 %d\n", write(fd, "123", 3));
	perror("write1 ");

	printf("lseek2 %d\n", lseek(fd, 7, SEEK_END));
  perror("lseek2 ");

  printf("write2 %d\n", write(fd, "456", 3));
  perror("write2 ");

  printf("lseek3 %d\n", lseek(fd, 10, SEEK_CUR));
  perror("lseek3 ");
  printf("write3 %d\n", write(fd, "789", 3));
  perror("write3 ");

	close(fd);
}
