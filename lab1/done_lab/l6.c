#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>
int main(int argc, char *argv[]) {
  int fd;

  fd=open(argv[1], O_CREAT | O_RDWR, 0666);
  perror("open ");
  printf("open %d\n", fd);

  if (fd < 0) {
    perror("Error step_1 ");
    return 1;
  }

	//ftruncate(fd, 1000);

  printf("lseek1 %d\n", lseek(fd, 100000, SEEK_SET));
  perror("lseek1 ");

  printf("write1 %d\n", write(fd, "HI", 2));
  perror("write1 ");

  close(fd);
}
