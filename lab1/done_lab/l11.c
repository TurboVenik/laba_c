
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    int max = -1;
    struct stat buf;
    for (int i = 1; i<argc; i++) {
        int fd;
        fd = open(argv[i],  O_RDWR);
        printf("1. OPEM new file fd is %d: %s\n", fd, argv[1]);
        if (fd == -1) {
            printf(" ERROR!\n fd is %d\n The text of Sys Error List is: %s\n", errno, sys_errlist[errno]);
            perror(" PError");
        }
        fstat(fd, &buf);
        if (max < buf.st_size)
            max = buf.st_size;
        close(fd);
    }
    printf("Max file size in arguments is %d bytes\n", max);
    return 0;
}
