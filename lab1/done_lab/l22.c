#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdio.h>
#include<errno.h>
#include<unistd.h>
#include<string.h>



int main(int argc, char *argv[]) {
  int fd;
  char data[14];
	mode_t mode;
	int perm = 0;
	int i;
  for (i = 0; i < strlen(argv[2]); i++) {
    int raw = argv[2][i] - '0';
		int form = 0;
		if (raw % 2 == 1) {
      form |= __S_IEXEC;
    }
		if ((raw / 2) % 2 == 1) {
			form |= __S_IWRITE;
    }
		if ((raw / 4) % 2 == 1) {
      form |= __S_IREAD;
    }
		perm |= form >> i * 3;
	}

  fd = open(argv[1],  O_CREAT | O_WRONLY, perm);
	if (fd < 0) {
    perror("Error step_1 ");
	}

	write(fd, "hello\nworld\n", 12);
  close(fd);

	fd=open(argv[1], O_RDONLY);
  if (fd < 0) {
    perror("Error step_2 ");
  }

  read(fd, data, 14);
  printf("%s", data);
  close(fd);


	fd=open(argv[1], O_RDWR);
  if (fd < 0) {
    perror("Error step_3 ");
  }

  close(fd);
  return 0;
}
