#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
//gcc -o target/l7_2 l7_2.c; gcc -o target/l7_1 l7_1.c; ./target/l7_2 files/file; ./target/l7_1 files/file
#define MAX_TRY 10

int main (int argc, char* argv[]) {

    int pid;
    pid = fork();
    if (pid == -1){
      perror("Error: ");
      return -1;
    }
    if (pid == 0){

      int fd;
      fd = open(argv[1], (O_WRONLY | O_CREAT | O_TRUNC), 0666);
      if(fd < 0){
          printf("\tError: %s\n", strerror(errno));
          return 1;
      }
      int count;
      printf("writer\n");
      struct flock wlock;
      char *str = "I am the writer. Nice to see you again.\n";
      wlock.l_whence = 0;
      wlock.l_start = 0L;
      wlock.l_len = 0;
      wlock.l_type = F_WRLCK;

      if (fcntl (fd, F_SETLK, &wlock) < 0) {
        perror("fcntl");
      }

      for(int i = 0; i < 2; ++i){
          sleep(1);
          count = write(fd, str, strlen(str));
          if (count < 0){
              printf("\tError: %s\n", strerror(errno));
              return -1;
          }
      }
      close(fd);
      return 0;
    } else {
      char* buffer;
      int fd;
      fd = open(argv[1], (O_RDONLY), 0666);
      if(fd < 0){
          printf("\tError: %s\n", strerror(errno));
          return 1;
      }
      int count;
      printf("reader\n");

      do {
          buffer = malloc(11);
          count = read(fd, buffer, 11);
          if (count < 0){
              printf("\tError: %s\n", strerror(errno));
              return -1;
          }
          if (count != 0) {
            printf("%s", buffer);
          }
          free(buffer);
      } while (count > 0);
      close(fd);
      return 0;
    }
}
