#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#define FIFO "fifo"
#define MAXBUFF 256

int main (int argc, char* argv[]) {
    int writefd;
    int count;
    char buffer[MAXBUFF];

    if ((writefd = open(FIFO, O_WRONLY)) < 0) {
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }

    if (write(writefd, "Hello\n", 6) != 6) {
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }
    close(writefd);

    if (unlink(FIFO) < 0){
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}