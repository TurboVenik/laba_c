#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#define FIFO "fifo"
#define MAXBUFF 256

void sighandler(int);

int main (int argc, char* argv[]) {
    struct sigaction act;
  act.sa_handler = sighandler;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;
  if (sigaction(SIGPIPE, &act, NULL) < 0){
    perror("Error: ");
    return 1;
  }

    int writefd;
    int count;
    char buffer[MAXBUFF];

    if ((writefd = open(FIFO, O_WRONLY)) < 0) {
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }
    int j = 0;
    while (1) {
      j++;
      printf("\t: %d\n", j);
      char * buffer;

      buffer = malloc(1000);
      int i;
      for (i = 0; i < 1000; i++) {
        buffer[i] = 1;
      }
      if (write(writefd, buffer, 1000) != 1000) {
          printf("\tError1: %s\n", strerror(errno));
          return -1;
      }
    }

    close(writefd);

    if (unlink(FIFO) < 0){
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void sighandler(int signum) {
	printf("Signal %d was there\n", signum);
	sleep(5);
	// struct sigaction act;
	// act.sa_handler = SIG_DFL;
	// if (sigaction(SIGINT, &act, NULL) < 0){
	// 	perror("Error: ");
	// 	return;
	// }
}
