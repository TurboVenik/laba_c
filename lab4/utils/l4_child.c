#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

#define FROM_FATHER 5
#define TO_FATHER 7

int copy(int, int);

int main (int argc, char* argv[]) {
    int i = 0;
    int count = 0;
    char buffer[1024];
    do {
        printf("[Son]: ");
        fflush(stdout);
        copy(1, TO_FATHER);

        //printf("Child before read\n");
        count = read(FROM_FATHER, buffer, 1024);
        //printf("Child count is %d\n", count);
        if (count < 0){
            printf("\tError: %s\n", strerror(errno));
            return -1;
        }
        buffer[count] = 0;
        printf("[Son] Father said me: %s", buffer);

        ++i;
    } while (i != 2);

    close(FROM_FATHER);
    close(TO_FATHER);

    return 0;
}

int copy(int from_fd, int to_fd)
{
    char buffer[1024];
    int r_count = 0;
    int w_count = 0;

    r_count = read(from_fd, buffer, 1024);
    //printf("R_count is %d\n", r_count);
    if (r_count < 0){
            printf("\tError: %s\n", strerror(errno));
            return -1;
    }
    w_count = write(to_fd, buffer, r_count);
    //printf("W_count is %d\n", w_count);
    if (w_count < 0){
            printf("\tError: %s\n", strerror(errno));
            return -1;
    }

    return 0;
}