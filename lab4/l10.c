#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
//gcc -o target/l7_2 l7_2.c; gcc -o target/l7_1 l7_1.c; ./target/l7_2 files/file; ./target/l7_1 files/file
#define MAX_TRY 10

int main (int argc, char* argv[]) {

    int pid;
    pid = fork();
    if (pid == -1){
      perror("Error: ");
      return -1;
    }
    if (pid == 0){
      while (1) {
        printf("NONONONONONO\n");
        sleep(1);
      }

      return 0;
    } else {
      char* buffer = malloc(1024);
      while (1) {
        scanf("%s", buffer);
        printf("String: %s\n",buffer);
      }

      return 0;
    }
}
