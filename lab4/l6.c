#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#define FIFO "./fifo"
#define MAXBUFF 256

void sighandler(int);

int main (int argc, char* argv[]) {

    struct sigaction act;

    act.sa_handler = sighandler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGPIPE, &act, NULL) < 0){
      perror("Error: ");
      return 1;
    }

    int readfd;
    int count;
    char buffer[MAXBUFF];

    if (mknod(FIFO, S_IFIFO | 0666, 0) < 0) {
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }

    if ((readfd = open(FIFO, O_RDONLY)) < 0) {
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }
    sleep(10000);
    // while ((count = read(readfd, buffer, MAXBUFF)) > 0)
    //     if (write(1, buffer, count) != count) {
    //         printf("\tError: %s\n", strerror(errno));
    //         return -1;
    //     }
    close(readfd);
    return 0;
}

void sighandler(int signum) {
	printf("Signal %d was there\n", signum);
	sleep(5);
	// struct sigaction act;
	// act.sa_handler = SIG_DFL;
	// if (sigaction(SIGINT, &act, NULL) < 0){
	// 	perror("Error: ");
	// 	return;
	// }
}
