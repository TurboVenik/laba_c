#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

int main (int argc, char* argv[]) {
		int child_to_parent[2];
  	int parent_to_child[2];
  	int cpid;

    if (pipe(child_to_parent) == -1){
    	perror("pipe");
        return 1;
    }

    if (pipe(parent_to_child) == -1){
        perror("pipe");
        return 1;
			}
    cpid = fork();
    if (cpid == -1) {
    	perror("fork");
    	return 1;
    }

    if (cpid == 0) {
        // close(child_to_parent[1]);
				char *str = "Hello, father0eq0eq0eq0eq0eq\n";
        close(parent_to_child[0]);
        // exit(2);
        char buffer[1024];
        int count = 0;
				write(child_to_parent[1], str, strlen(str) + 1);
        count = read(child_to_parent[0], buffer, 1024);

        printf("%s", buffer);
        close(child_to_parent[0]);


        write(parent_to_child[1], str, strlen(str) + 1);
        close(parent_to_child[1]);

        return 0;
    } else {
        close(child_to_parent[0]);
        close(parent_to_child[1]);

        char *str = "\tHello, my boy\n";
        write(child_to_parent[1], str, strlen(str) + 1);
        close(child_to_parent[1]);

        int count = 0;
        char buffer[1024];

        count = read(parent_to_child[0], buffer, 1024);

        printf("%s", buffer);
        close(parent_to_child[0]);

        wait(NULL);
        return 0;
    }
}
