#define MAXBUFF   80
#define PERM     0666

typedef struct mem_msg {
  int segment;
  char buff[MAXBUFF];
} Message;

static struct sembuf proc_wait[1] = {
  1, -1, 0
};

static struct sembuf proc_start[1] = {
  1, 1, 0
};

static struct sembuf mem_lock[2] = {
  0, 0, 0,
  0, 1, 0
};

static struct sembuf mem_unlock[1] = {
  0, -1, 0
};

static struct sembuf server_wait[1] = {
        2, -1, 0
};

static struct sembuf server_start[1] = {
        2, 1, 0
};

static struct sembuf end_wait[1] = {
        3, -1, 0
};

static struct sembuf end_start[1] = {
        3, 1, 0
};
