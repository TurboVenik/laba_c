#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>

#include "shmem.h"

int main(int argc, char* argv[]) {
    Message *msgptr;
    key_t key;
    int shmid, semid;

    key = atoi(argv[1]);

//    if ( (key = ftok("server", 'P')) < 0) {
//        printf("Невозможно получить ключ\n");exit(1);
//    }



    if ( (shmid = shmget(key, sizeof(Message) , 0)) < 0) {
        printf("Ошибка доступа\n");exit(1);
    }

    if ( (msgptr = (Message *)shmat(shmid, 0, 0)) < 0) {
        printf("Ошибка присоединения\n");exit(1);
    }

    if ( (semid = semget(key,4, PERM)) < 0) {
        printf("Ошибка доступа\n");exit(1);
    }

    if (semop (semid,&mem_lock[0], 2) < 0) {
        printf("Невозможно выполнить операцию1\n");exit(1);
    }
    if (semop (semid,&proc_start[0], 1) < 0) {
        printf("Невозможно выполнить операцию2\n");exit(1);
    }
    sprintf(msgptr->buff, argv[2]);

    if (semop (semid,&mem_unlock[0], 1) < 0) {
        printf("Невозможно выполнить операцию3\n");exit(1);
    }
    if (semop (semid,&server_wait[0], 1) < 0) {
        printf("Невозможно выполнить операцию4\n");exit(1);
    }
    if (semop (semid,&mem_lock[0], 2) < 0) {
        printf("Невозможно выполнить операцию5\n");exit(1);
    }

    printf("(%s << 1) + (%s >> 1) = %s\n", argv[2], argv[2], msgptr -> buff);

    if (semop (semid,&mem_unlock[0], 1) < 0) {
        printf("Невозможно выполнить операцию6\n");exit(1);
    }
    if (semop (semid,&end_start[0], 1) < 0) {
        printf("Невозможно выполнить операцию2\n");exit(1);
    }

    exit(0);
}