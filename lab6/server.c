#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <time.h>
#include <stdlib.h>

#include "shmem.h"

#define MAX_SOCK_BUF 256

int node1();
int node2();
int node3();

int main() {
    srand(time(NULL));
    Message *msgptr;
    key_t key;
    int shmid, semid;

    if ((key = ftok("server", 'P')) < 0) {
        perror("Невозможно получить ключ");
        exit(1);
    }


    if ((shmid = shmget(key, sizeof(Message), PERM | IPC_CREAT)) < 0) {
        perror("Невозможно создать область");
        exit(1);
    }

    if ((msgptr = (Message *) shmat(shmid, 0, 0)) < 0) {
        perror("Ошибка присоединения");
        exit(1);
    }

    if ((semid = semget(key, 4, PERM | IPC_CREAT)) < 0) {
        perror("Невозможно создать семафор");
        exit(1);
    }

    if (semop(semid, &proc_wait[0], 1) < 0) {
        perror("Невозможно выполнить операцию");
        exit(1);
    }

    if (semop(semid, &mem_lock[0], 2) < 0) {
        perror("Невозможно выполнить операцию");
        exit(1);
    }

    int number = atoi(msgptr->buff);
    number = number * 2;

    if (semop(semid, &server_start[0], 1) < 0) {
        perror("Невозможно выполнить операцию");
        exit(1);
    }
//!!
    char buf[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        perror("Невозможно создать сокет");
        exit(1);
    }

    unlink("./echo.server");
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");
    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, saddrlen) < 0) {
        perror("Ошибка связывания сокета1");
        exit(1);
    }

    int pid;
    pid = fork();
    if (pid == -1) {
        perror("Error: ");
        exit(1);
    }

    if (pid == 0) {
        node1();
    }

    pid = fork();
    if (pid == -1) {
        perror("Error: ");
        exit(1);
    }

    if (pid == 0) {
        node2();
    }

    pid = fork();
    if (pid == -1) {
        perror("Error: ");
        exit(1);
    }

    if (pid == 0) {
        node3();
    }


    max_caddrlen = sizeof(clnt_addr);
    caddrlen = max_caddrlen;
    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0,
                      (struct sockaddr *) &clnt_addr, &caddrlen) < 0)) {
        exit(1);
    }

    int lenn;
    lenn = strlen(msgptr->buff);

    fflush(0);
    if (sendto(sockfd, msgptr->buff, lenn, 0,
               (struct sockaddr *) &clnt_addr, saddrlen) != lenn) {
        perror("Ошибка передачи сообщения");
        exit(1);
    }



    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0,
                      (struct sockaddr *) &clnt_addr, &caddrlen) < 0)) {
        exit(1);
    }

    lenn = strlen(msgptr->buff);

    fflush(0);
    if (sendto(sockfd, msgptr->buff, lenn, 0,
               (struct sockaddr *) &clnt_addr, saddrlen) != lenn) {
        perror("Ошибка передачи сообщения");
        exit(1);
    }



    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0,
                      (struct sockaddr *) &clnt_addr, &caddrlen) < 0)) {
        exit(1);
    }


    number = atoi(buf);

    int length = snprintf(NULL, 0, "%d", number);
    char *str = malloc(length + 1);
    snprintf(str, length + 1, "%d", number);

    sprintf(msgptr->buff, str);

    if (semop(semid, &mem_unlock[0], 1) < 0) {
        perror("Невозможно выполнить операцию 48");
        exit(1);
    }

    if (semop(semid, &end_wait[0], 1) < 0) {
        perror("Невозможно выполнить операцию");
        exit(1);
    }

    if (shmdt(msgptr) < 0) {
        perror("Невозможно выполнить операцию shmdt");
        exit(1);
    }

    if (shmctl(shmid, IPC_RMID, 0) < 0) {
        perror("Невозможно удалить область");
        exit(1);
    }

    if (semctl(semid, 0, IPC_RMID) < 0) {
        perror("Невозможно удалить семафор");
        exit(1);
    }
}

int node1() {
    char *msg = "ping";
    char buf[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr, node3_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n, msglen;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        perror("Невозможно создать сокет");
        exit(1);
    }

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");


    bzero(&clnt_addr, sizeof(clnt_addr));
    clnt_addr.sun_family = AF_UNIX;
    strcpy(clnt_addr.sun_path, "/tmp/clnt_1");
    mkstemp(clnt_addr.sun_path);

    saddrlen = sizeof(clnt_addr.sun_family) + strlen(clnt_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &clnt_addr, saddrlen) < 0) {
        perror("Ошибка связывания сокета2");
//        exit(1);
    }

    msglen = strlen(msg);

    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);
    if (sendto(sockfd, msg, msglen, 0,
               (struct sockaddr *) &serv_addr, saddrlen) != msglen) {
        perror("Ошибка передачи сообщения");
        perror("LOL");
//        exit(1);
    }

    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
        perror("Ошибка получения сообщения");
//        exit(1);
    };

    int number = atoi(buf);

    printf("\tnode1 '%d'\n",number);

    number = number << 1;

    printf("\tnode1 '%d'\n",number);

    int r = rand() % 100;

    usleep(r * 1000);

    bzero(&node3_addr, sizeof(node3_addr));
    node3_addr.sun_family = AF_UNIX;
    strcpy(node3_addr.sun_path, "/tmp/clnt_3");

    saddrlen = sizeof(node3_addr.sun_family) + strlen(node3_addr.sun_path);


    int length = snprintf(NULL, 0, "%d", number) + 1;
    char *str = malloc(length);
    snprintf(str, length, "%d", number);

    printf("\tnode1 '%s'\n",str);

    if (sendto(sockfd, str, strlen(str), 0,
               (struct sockaddr *) &node3_addr, saddrlen) != strlen(str)) {
        perror("Ошибка передачи сообщения");
//        exit(1);
    }

    close(sockfd);
    unlink(clnt_addr.sun_path);
    exit(0);
}

int node2() {
    char *msg = "ping";
    char buf[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr, node3_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n, msglen;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        perror("Невозможно создать сокет");
        exit(1);
    }

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");


    bzero(&clnt_addr, sizeof(clnt_addr));
    clnt_addr.sun_family = AF_UNIX;
    strcpy(clnt_addr.sun_path, "/tmp/clnt_2");
    mkstemp(clnt_addr.sun_path);

    saddrlen = sizeof(clnt_addr.sun_family) + strlen(clnt_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &clnt_addr, saddrlen) < 0) {
        perror("Ошибка связывания сокета3");
        exit(1);
    }

    msglen = strlen(msg);

    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);
    if (sendto(sockfd, msg, msglen, 0,
               (struct sockaddr *) &serv_addr, saddrlen) != msglen) {
        perror("Ошибка передачи сообщения");
        exit(1);
    }

    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
        perror("Ошибка получения сообщения");
        exit(1);
    };

    int number = atoi(buf);

    printf("node2 '%d'\n",number);

    number = number >> 1;

    printf("node2 '%d'\n",number);

    int r = rand() % 100;

    usleep(r * 1000);

    bzero(&node3_addr, sizeof(node3_addr));
    node3_addr.sun_family = AF_UNIX;
    strcpy(node3_addr.sun_path, "/tmp/clnt_3");

    saddrlen = sizeof(node3_addr.sun_family) + strlen(node3_addr.sun_path);

    int length = snprintf(NULL, 0, "%d", number) + 1;
    char *str = malloc(length);
    snprintf(str, length, "%d", number);

    printf("node2 '%s' '%d'\n",str, strlen(str));

    if (sendto(sockfd, str, strlen(str), 0,
               (struct sockaddr *) &node3_addr, saddrlen) != strlen(str)) {
        perror("Ошибка передачи сообщения");
        exit(1);
    }



    close(sockfd);

    unlink(clnt_addr.sun_path);
    exit(0);
}

int node3() {
    char *msg = "lolkek";
    char buf[MAX_SOCK_BUF];
    char buf1[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n, msglen;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        perror("Невозможно создать сокет");
        exit(1);
    }

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");


    bzero(&clnt_addr, sizeof(clnt_addr));
    clnt_addr.sun_family = AF_UNIX;
    strcpy(clnt_addr.sun_path, "/tmp/clnt_3");
    mkstemp(clnt_addr.sun_path);

    saddrlen = sizeof(clnt_addr.sun_family) + strlen(clnt_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &clnt_addr, saddrlen) < 0) {
        perror("Ошибка связывания сокета4");
        exit(1);
    }

    msglen = strlen(msg);

    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);

    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
        perror("Ошибка получения сообщения");
        exit(1);
    };

    int sum;
    sum = atoi(buf);

    printf("\t\tnode3 '%d'\n",sum);

    if ((n = recvfrom(sockfd, buf1, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
        perror("Ошибка получения сообщения");
        exit(1);
    };

    sum += atoi(buf1);
    printf("\t\tnode3 '%s' '%d'\n",buf1, atoi(buf1));

    printf("\t\tnode3 '%d'\n",sum);

    int length = snprintf(NULL, 0, "%d", sum) + 1;
    char *str = malloc(length);
    snprintf(str, length, "%d", sum);

    if (sendto(sockfd, str, strlen(str), 0,
               (struct sockaddr *) &serv_addr, saddrlen) != strlen(str)) {
        perror("Ошибка передачи сообщения");
        exit(1);
    }


    close(sockfd);
    unlink(clnt_addr.sun_path);
    exit(0);
}
