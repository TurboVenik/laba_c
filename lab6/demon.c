#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "shmem.h"

#define MAX_SOCK_BUF 256

int node1();

int node2();

int node3();

int readConfig();

int readLine(int fd, char *buffer, int i);

void SIGHUPHandler(int);
void SIGTERMHandler(int);
void SIGCHLDHandler(int);

int deleteOldIpc();

int createNewIpc();

key_t key;

int logFd;

Message *msgptr;

int shmid, semid;

int writeln(int number);
int print(int number);

int main(int argc, char **argv) {
    int fd, number;
    struct rlimit flim;

    getrlimit(RLIMIT_NOFILE, &flim);
    for (fd = 0; fd < flim.rlim_max; fd++) {
        close(fd);
    }

    srand(time(NULL));

    if (getppid() != 1) {
        signal(SIGTTOU, SIG_IGN);
        signal(SIGTTIN, SIG_IGN);
        signal(SIGTSTP, SIG_IGN);

        if (fork() != 0) {
            exit(0);
        }

        setsid();

        readConfig();

    } else {
        readConfig();
    }

    char buf[MAX_SOCK_BUF];
    char buf1[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
    }

    unlink("./echo.server");
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");
    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, saddrlen) < 0) {
    }


    struct sigaction act;

    act.sa_handler = SIGHUPHandler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGHUP, &act, NULL);


    struct sigaction act1;

    act1.sa_handler = SIGTERMHandler;
    sigemptyset(&act1.sa_mask);
    act1.sa_flags = 0;
    sigaction(SIGTERM, &act1, NULL);

    struct sigaction act2;

    act2.sa_handler = &SIGCHLDHandler;
    sigemptyset(&act2.sa_mask);
    act2.sa_flags = SA_NOCLDWAIT;
    if (sigaction(SIGCHLD, &act2, NULL) < 0);

    int asd = 0;
    while (1) {
        asd ++;
        int a;

//        write(logFd, "Ожидание операции № ", strlen("Ожидание операции № "));
//        writeln(asd);

        if (a = semop(semid, &proc_wait[0], 1) < 0) {
            continue;
        }
        if (semop(semid, &mem_lock[0], 2) < 0) {
            continue;
        }
        if (semop(semid, &server_start[0], 1) < 0) {
            continue;
        }

        int pid;
        pid = fork();

        if (pid == -1) {
            continue;
        }

        if (pid == 0) {
            node1();
        }

        pid = fork();
        if (pid == -1) {
            continue;
        }

        if (pid == 0) {
            node2();
        }

        pid = fork();
        if (pid == -1) {
            continue;
        }

        if (pid == 0) {
            node3();
        }

        max_caddrlen = sizeof(clnt_addr);
        caddrlen = max_caddrlen;
        if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0,
                          (struct sockaddr *) &clnt_addr, &caddrlen) < 0)) {
            continue;
        }

        int lenn;
        lenn = strlen(msgptr->buff);

        write(logFd, "Новое число для обработки: ", strlen("Новое число для обработки: "));
        write(logFd, msgptr->buff, strlen(msgptr->buff));
        write(logFd, "\n", 1);

        if (sendto(sockfd, msgptr->buff, lenn, 0,
                   (struct sockaddr *) &clnt_addr, saddrlen) != lenn) {
            continue;
        }

        if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0,
                          (struct sockaddr *) &clnt_addr, &caddrlen) < 0)) {
            continue;
        }

        lenn = strlen(msgptr->buff);


        if (sendto(sockfd, msgptr->buff, lenn, 0,
                   (struct sockaddr *) &clnt_addr, saddrlen) != lenn) {
            continue;
        }

        if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0,
                          (struct sockaddr *) &clnt_addr, &caddrlen) < 0)) {
            continue;
        }

        int number;
        number = atoi(buf);

        int length = snprintf(NULL, 0, "%d", number);
        char *str = malloc(length + 1);
        snprintf(str, length + 1, "%d", number);

        write(logFd, "Ответ: ", strlen("Ответ: "));
        writeln(number);

        sprintf(msgptr->buff, str);

        if (semop(semid, &mem_unlock[0], 1) < 0) {
            continue;
        }

        if (semop(semid, &end_wait[0], 1) < 0) {
            continue;
        }
    }
}

int node1() {
    char *msg = "ping";
    char buf[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr, node3_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n, msglen;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
    }

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");


    bzero(&clnt_addr, sizeof(clnt_addr));
    clnt_addr.sun_family = AF_UNIX;
    strcpy(clnt_addr.sun_path, "/tmp/clnt_1");
    mkstemp(clnt_addr.sun_path);

    saddrlen = sizeof(clnt_addr.sun_family) + strlen(clnt_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &clnt_addr, saddrlen) < 0) {
    }

    msglen = strlen(msg);

    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);
    if (sendto(sockfd, msg, msglen, 0,
               (struct sockaddr *) &serv_addr, saddrlen) != msglen) {
    }

    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
    }

    int number = atoi(buf);

    number = number << 1;

    int r = rand() % 100;

    usleep(r * 1000);

    bzero(&node3_addr, sizeof(node3_addr));
    node3_addr.sun_family = AF_UNIX;
    strcpy(node3_addr.sun_path, "/tmp/clnt_3");

    saddrlen = sizeof(node3_addr.sun_family) + strlen(node3_addr.sun_path);


    int length = snprintf(NULL, 0, "%d", number) + 1;
    char *str = malloc(length);
    snprintf(str, length, "%d", number);

    if (sendto(sockfd, str, strlen(str), 0,
               (struct sockaddr *) &node3_addr, saddrlen) != strlen(str)) {

    }

    close(sockfd);
    unlink(clnt_addr.sun_path);
    exit(0);
}

int node2() {
    char *msg = "ping";
    char buf[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr, node3_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n, msglen;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        exit(1);
    }

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");


    bzero(&clnt_addr, sizeof(clnt_addr));
    clnt_addr.sun_family = AF_UNIX;
    strcpy(clnt_addr.sun_path, "/tmp/clnt_2");
    mkstemp(clnt_addr.sun_path);

    saddrlen = sizeof(clnt_addr.sun_family) + strlen(clnt_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &clnt_addr, saddrlen) < 0) {
        exit(1);
    }

    msglen = strlen(msg);

    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);
    if (sendto(sockfd, msg, msglen, 0,
               (struct sockaddr *) &serv_addr, saddrlen) != msglen) {
        exit(1);
    }

    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
        exit(1);
    };

    int number = atoi(buf);

    number = number >> 1;

    int r = rand() % 100;

    usleep(r * 1000);

    bzero(&node3_addr, sizeof(node3_addr));
    node3_addr.sun_family = AF_UNIX;
    strcpy(node3_addr.sun_path, "/tmp/clnt_3");

    saddrlen = sizeof(node3_addr.sun_family) + strlen(node3_addr.sun_path);

    int length = snprintf(NULL, 0, "%d", number) + 1;
    char *str = malloc(length);
    snprintf(str, length, "%d", number);

    if (sendto(sockfd, str, strlen(str), 0,
               (struct sockaddr *) &node3_addr, saddrlen) != strlen(str)) {
        exit(1);
    }

    close(sockfd);

    unlink(clnt_addr.sun_path);
    exit(0);
}

int node3() {
    char *msg = "lolkek";
    char buf[MAX_SOCK_BUF];
    char buf1[MAX_SOCK_BUF];

    struct sockaddr_un serv_addr, clnt_addr;
    int sockfd;
    int saddrlen, caddrlen, max_caddrlen, n, msglen;

    if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        exit(1);
    }

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, "./echo.server");


    bzero(&clnt_addr, sizeof(clnt_addr));
    clnt_addr.sun_family = AF_UNIX;
    strcpy(clnt_addr.sun_path, "/tmp/clnt_3");
    mkstemp(clnt_addr.sun_path);

    saddrlen = sizeof(clnt_addr.sun_family) + strlen(clnt_addr.sun_path);

    if (bind(sockfd, (struct sockaddr *) &clnt_addr, saddrlen) < 0) {
        exit(1);
    }

    msglen = strlen(msg);

    saddrlen = sizeof(serv_addr.sun_family) + strlen(serv_addr.sun_path);

    if ((n = recvfrom(sockfd, buf, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
        exit(1);
    };

    int sum;
    sum = atoi(buf);

    if ((n = recvfrom(sockfd, buf1, MAX_SOCK_BUF, 0, NULL, 0) < 0)) {
        exit(1);
    };

    sum += atoi(buf1);


    int length = snprintf(NULL, 0, "%d", sum) + 1;
    char *str = malloc(length);
    snprintf(str, length, "%d", sum);

    if (sendto(sockfd, str, strlen(str), 0,
               (struct sockaddr *) &serv_addr, saddrlen) != strlen(str)) {
        exit(1);
    }


    close(sockfd);
    unlink(clnt_addr.sun_path);
    exit(0);
}

int readConfig() {
    int fd, n;
    char port[100];
    char logfile[100];

    fd = open("config.txt", O_RDWR);
    if (fd < 0) {
    }

    int i;
    i = readLine(fd, port, 0);

    i = readLine(fd, logfile, i);

    logFd = open(logfile, (O_APPEND | O_RDWR | O_CREAT), 0666);
    if (logFd < 0) {
    }

    n = write(logFd, "Импортирован конфиг\n", strlen("Импортирован конфиг\n"));
    if (n < 0) {
    }

    if ((key = ftok(logfile, 'H')) < 0) {

    }

    write(logFd, "Новые параметры демона (PID, KEY): ", strlen("Новые параметры демона (PID, KEY): "));
    print(getpid());
    write(logFd, ", ", strlen(", "));
    writeln(key);

    deleteOldIpc();

    createNewIpc();

    close(fd);
    return 0;
}

int createNewIpc(){
    if ((shmid = shmget(key, sizeof(Message), PERM | IPC_CREAT)) < 0) {
    }
    if ((msgptr = (Message *) shmat(shmid, 0, 0)) < 0) {
    }
    if ((semid = semget(key, 4, PERM | IPC_CREAT)) < 0) {
    }
}

int deleteOldIpc() {

    if (shmdt(msgptr) < 0) {
    }
    if (shmctl(shmid, IPC_RMID, 0) < 0) {
    }
    if (semctl(semid, 0, IPC_RMID) < 0) {
    }
}

int readLine(int fd, char *buffer, int i) {
    struct stat buf;
    fstat(fd, &buf);
    int size = buf.st_size;

    char tmp[1];

    int j;
    j = 0;

    int f = 0;
    while (i < size) {
        i++;

        read(fd, tmp, 1);

        if (tmp[0] == '\n') {
            break;
        }
        if (tmp[0] == '=') {
            f = 1;
            continue;
        }

        if (!f) {
            continue;
        }

        buffer[j] = tmp[0];
        buffer[j + 1] = '\0';

        j++;
    }

    return i;
}

void SIGHUPHandler(int signum) {
    readConfig();
}

void SIGTERMHandler(int signum) {
    deleteOldIpc();
    while(wait() > 0);

    write(logFd, "Демон завершает работу\n", strlen("Демон завершает работу\n"));
    exit(2);
}

int writeln(int number) {
    int length = snprintf(NULL, 0, "%d", number);
    char *str = malloc(length + 1);
    snprintf(str, length + 1, "%d", number);

    write(logFd, str, strlen(str));
    write(logFd, "\n", 1);
}


int print(int number) {
    int length = snprintf(NULL, 0, "%d", number);
    char *str = malloc(length + 1);
    snprintf(str, length + 1, "%d", number);

    write(logFd, str, strlen(str));
}

void SIGCHLDHandler(int signum) {
    wait(NULL);
}