#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>


int vivod(int fd) {
    struct stat buf;
    char* buffer;
    int i;
    int length;
    fstat(fd, &buf);
    int size = buf.st_size;
    buffer = malloc(1);
    lseek(fd,-1,SEEK_END);
    for (i=0; i < size; i++) {
        read(fd, buffer, 1);
        printf("%s", buffer);
        lseek(fd,-2,SEEK_CUR);
    }
    free(buffer);
}

int main(int argc, char **argv)
{
    int fd;
    fd = open(argv[1],  O_RDWR);
    printf("1. OPEM new file fd is %d: %s\n", fd, argv[1]);
    if (fd == -1) {
        printf(" ERROR!\n fd is %d\n The text of Sys Error List is: %s\n", errno, sys_errlist[errno]);
        perror(" PError");
    }
    vivod(fd);
    close(fd);
    return 0;
}
