#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

int copy(int, int);

int main (int argc, char* argv[]) {
    int to_child[2];
    int to_father[2];
    int cpid;

    if (pipe(to_child) == -1){
        perror("pipe");
        return 1;
    }

    if (pipe(to_father) == -1){
        perror("pipe");
        return 1;
    }

    cpid = fork();
    if (cpid == -1) {
        perror("fork");
        return 1;
    }

    if (cpid == 0) {    /* Child reads from pipe */
        close(to_child[1]);          /* Close unused write end */
        close(to_father[0]);

        //printf("In child\n");

        int i = 0;
        int count = 0;
        char buffer[1024];
        do {
            printf("[Son]: ");
            fflush(stdout);
            copy(1, to_father[1]);

            //printf("Child before read\n");
            count = read(to_child[0], buffer, 1024);
            //printf("Child count is %d\n", count);
            if (count < 0){
                printf("\tError: %s\n", strerror(errno));
                return -1;
            }
            buffer[count] = 0;
            printf("[Son] Father said me: %s", buffer);

            ++i;
        } while (i != 2);

        close(to_father[1]);
        close(to_child[0]);

        return 0;


    } else {        
        close(to_child[0]);          /* Close unused read end */
        close(to_father[1]);         /* Close unused read end */

        //printf("In father\n");

        int count = 0;
        char buffer[1024];
        int i = 0;
        do {
            //printf("Father before read\n");
            count = read(to_father[0], buffer, 1024);
            //printf("Father count is %d\n", count);
            if (count < 0){
                printf("\tError: %s\n", strerror(errno));
                return -1;
            }
            buffer[count] = 0;
            printf("[Father] Son said me: %s", buffer);

            printf("[Father]: ");
            fflush(stdout);
            copy(1, to_child[1]);
            ++i;
        }
        while (i != 2);

        close(to_father[0]);
        close(to_child[1]);

        wait(NULL);             /* Wait for child */
        return 0;
    }
}

int copy(int from_fd, int to_fd)
{
    char buffer[1024];
    int r_count = 0;
    int w_count = 0;

    r_count = read(from_fd, buffer, 1024);
    //printf("R_count is %d\n", r_count);
    if (r_count < 0){
            printf("\tError: %s\n", strerror(errno));
            return -1;
    }
    w_count = write(to_fd, buffer, r_count);
    //printf("W_count is %d\n", w_count);
    if (w_count < 0){
            printf("\tError: %s\n", strerror(errno));
            return -1;
    }

    return 0;
}