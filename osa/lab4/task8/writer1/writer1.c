#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

int main (int argc, char* argv[]) {
    int fd;
    fd = open(argv[1], (O_WRONLY), 0666);
    if(fd < 0){
        printf("\tError: %s\n", strerror(errno));
        return 1;
    }
    int count;
    struct flock wlock;
    char *str = "I am the writer 1. My name is Gary Topor.\n";
    for(int i = 0; i < 700000; ++i){

        wlock.l_whence = SEEK_SET;
        wlock.l_start = 0;
        wlock.l_len = 0;
        wlock.l_type = F_WRLCK;
        fcntl(fd, F_SETLKW, &wlock);

        count = write(fd, str, strlen(str) + 1);
        if (count < 0){
            printf("\tError: %s\n", strerror(errno));
            return -1;
        }

        wlock.l_type = F_UNLCK;
        fcntl(fd, F_SETLKW, &wlock);
    }
    close(fd);
    return 0;
}