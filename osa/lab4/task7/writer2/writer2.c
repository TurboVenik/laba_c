#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

int main (int argc, char* argv[]) {
    int fd;
    fd = open(argv[1], (O_WRONLY), 0666);
    if(fd < 0){
        printf("\tError: %s\n", strerror(errno));
        return 1;
    }
    int count;
    char *str = "I am R2D2 and I am second writer.\n";
    for(int i = 0; i < 700000; ++i){
        count = write(fd, str, strlen(str) + 1);
        if (count < 0){
            printf("\tError: %s\n", strerror(errno));
            return -1;
        }
    }
    close(fd);
    return 0;
}