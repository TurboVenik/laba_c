#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

int main (int argc, char* argv[]) {
    char buffer[1024];
    int r_count;
    int w_count;
    int pid;
    struct flock wlock;

    pid = fork();
    if (pid < 0) {
        printf("\tError: %s\n", strerror(errno));
        return 1;
    }
    if (pid == 0) {
        do {
            wlock.l_whence = SEEK_SET;
            wlock.l_start = 0;
            wlock.l_len = 0;
            wlock.l_type = F_WRLCK;
            fcntl(0, F_SETLKW, &wlock);

            printf("Child reads:\n");
            fflush(stdout);

            r_count = read(0, buffer, 1024);
            if (r_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }

            wlock.l_type = F_UNLCK;
            fcntl(0, F_SETLKW, &wlock);

            printf("Child writes:\n");
            fflush(stdout);

            wlock.l_whence = SEEK_SET;
            wlock.l_start = 0;
            wlock.l_len = 0;
            wlock.l_type = F_WRLCK;
            fcntl(1, F_SETLKW, &wlock);

            w_count = write(1, buffer, r_count);
            if (w_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }

            wlock.l_type = F_UNLCK;
            fcntl(1, F_SETLKW, &wlock);

        } while (r_count != 0);
    }
    else {
        do {

            wlock.l_whence = SEEK_SET;
            wlock.l_start = 0;
            wlock.l_len = 0;
            wlock.l_type = F_WRLCK;
            fcntl(0, F_SETLKW, &wlock);

            printf("\tParent reads:\n");
            fflush(stdout);

            r_count = read(0, buffer, 1024);
            if (r_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }

            wlock.l_type = F_UNLCK;
            fcntl(0, F_SETLKW, &wlock);

            printf("\tParent writes:\n");
            fflush(stdout);

            wlock.l_whence = SEEK_SET;
            wlock.l_start = 0;
            wlock.l_len = 0;
            wlock.l_type = F_WRLCK;
            fcntl(1, F_SETLKW, &wlock);

            w_count = write(1, buffer, r_count);
            if (w_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }

            wlock.l_type = F_UNLCK;
            fcntl(1, F_SETLKW, &wlock);

        } while (r_count != 0);
        wait(NULL);
    }
    return 0;
}
