#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#define FIFO "./fifo"
#define MAXBUFF 256

int main (int argc, char* argv[]) {
    int readfd;
    int count;
    char buffer[MAXBUFF];

    if (mknod(FIFO, S_IFIFO | 0666, 0) < 0) {
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }

    if ((readfd = open(FIFO, O_RDONLY)) < 0) {
        printf("\tError: %s\n", strerror(errno));
        return -1;
    }

    while ((count = read(readfd, buffer, MAXBUFF)) > 0)
        if (write(1, buffer, count) != count) {
            printf("\tError: %s\n", strerror(errno));
            return -1;
        }
    close(readfd);
    return 0;
}