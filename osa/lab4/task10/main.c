#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

int main (int argc, char* argv[]) {
    char buffer[1024];
    int r_count;
    int w_count;
    int pid;
    pid = fork();
    if (pid < 0) {
        printf("\tError: %s\n", strerror(errno));
        return 1;
    }
    if (pid == 0) {
        do {
            r_count = read(0, buffer, 1024);
            if (r_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }
            printf("From child:\n");
            w_count = write(1, buffer, r_count);
            if (w_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }

        } while (r_count != 0);
    }
    else {
        do {
            r_count = read(0, buffer, 1024);
            if (r_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }
            printf("From parent:\n");
            w_count = write(1, buffer, r_count);
            if (w_count < 0) {
                printf("\tError: %s\n", strerror(errno));
                return 1;
            }

        } while (r_count != 0);
        wait(NULL);
    }
    return 0;
}