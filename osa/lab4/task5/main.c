#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

int main (int argc, char* argv[]) {
    int to_child[2];
    int cpid;

    if (pipe(to_child) == -1){
        perror("pipe");
        return 1;
    }

    cpid = fork();
    if (cpid == -1) {
        perror("fork");
        return 1;
    }

    if (cpid == 0) {    /* Child reads from pipe */
        close(to_child[1]);          /* Close unused write end */
        dup2(to_child[0], 0);
        execl("/usr/bin/wc", "wc", "-l", NULL);

        return 0;


    } else {
        close(to_child[0]);
        dup2(to_child[1], 1);
        execl("/usr/bin/who", "who", NULL);
        wait(NULL);
    }
}
