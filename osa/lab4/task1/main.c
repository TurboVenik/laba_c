#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

int main (int argc, char* argv[]) {
	int to_child[2];
    int to_father[2];
    int cpid;

    if (pipe(to_child) == -1){
    	perror("pipe");
        return 1;
    }

    if (pipe(to_father) == -1){
        perror("pipe");
        return 1;
    }

    cpid = fork();
    if (cpid == -1) {
    	perror("fork");
    	return 1;
    }

    if (cpid == 0) { 
        close(to_child[1]);
        close(to_father[0]);

        char buffer[1024];
        int count = 0;

        do {
            count = read(to_child[0], buffer, 1024);
        } while (count == 0);

        printf("Father said: %s", buffer);
        close(to_child[0]);

        char *str = "My father is a good man\n";
        write(to_father[1], str, strlen(str) + 1);
        close(to_father[1]);

        return 0;


    } else {
        close(to_child[0]);      
        close(to_father[1]);

        char *str = "My son is a good boy\n";
        write(to_child[1], str, strlen(str) + 1);
        close(to_child[1]);

        int count = 0;
        char buffer[1024];
        do {
            count = read(to_father[0], buffer, 1024);
        } while (count == 0);

        printf("Son said: %s", buffer);
        close(to_father[0]);

        wait(NULL);
        return 0;
    }
}