#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

void sighandler(int);

int main (int argc, char* argv[]) {
	if (signal(SIGINT, sighandler) == SIG_ERR)
		printf("I can't catch SIGINT\n");

	while(1) {
		printf("Going to sleep for a second...\n");
		sleep(1);
	}
	return(0);
}

void sighandler(int signum) {
	if (signal(SIGINT, SIG_DFL) == SIG_ERR)
		printf("I can't catch SIGINT\n");
	printf("Caught signal %d, coming out...\n", signum);
}