#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (int argc, char* argv[]) {
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("Child pid is %d\n", getpid());
		for (int i = 0; i < 100; ++i){
			for (int j = 0; j < 1000; ++j){
				for (int k = 0; k < 10000; ++k){
				;
				}
			}
			printf("Cycle number is %d\n", i);
		}
		printf("Child has finished\n");
		exit(0);
	}
	else {
		printf("Parent pid is %d\n", getpid());
		sleep(1);
		int res = kill(pid, SIGINT);
		if (res == -1){
			perror("Error: ");
			return -1;
		}
		printf("I sent SIGINT to my son\n");
		int status;
		int pid;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}
		printf("Child with PID %d returned status code %d\n", pid, WEXITSTATUS(status));
		if (WIFSIGNALED(status)){
			printf("Child: I recieved signal no. %d\n", WTERMSIG(status));
		}
		else
			printf("Child: I didn't recieve any signals\n");	
			printf("Parent is gone\n");
	}
	return 0;
}