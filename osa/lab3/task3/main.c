#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

void sighandler(int);

int main (int argc, char* argv[]) {
	if (signal(SIGCHLD, sighandler) == SIG_ERR)
		printf("I can't catch SIGINT\n");

	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("Child pid is %d\n", getpid());
		sleep(5);
		printf("Child is gone\n");
		exit(0);
	}
	else {
		printf("Parent pid is %d\n", getpid());
		sleep(10);
		printf("Parent is gone\n");
	}
	return 0;
}

void sighandler(int signum) {
	pid_t pid;
	pid = wait(NULL);
	printf("Pid %d exited.\n", pid);
}