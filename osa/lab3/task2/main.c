#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

void sighandler(int, siginfo_t*, void*);

int main (int argc, char* argv[]) {
	struct sigaction act;
	memset(&act, '\0', sizeof(act));
	act.sa_sigaction = &sighandler;
	if (sigaction(SIGINT, &act, NULL) < 0){
		perror("Error: ");
		return 1;
	}

	while(1) {
		printf("Going to sleep for a second...\n");
		sleep(1);
	}
	return(0);
}

void sighandler(int signum, siginfo_t *siginfo, void *context) {
	if (signal(SIGINT, SIG_DFL) == SIG_ERR)
		printf("I can't catch SIGINT\n");
	printf("Caught signal %d, coming out...\n", signum);
}