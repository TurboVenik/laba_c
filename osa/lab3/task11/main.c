#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/wait.h>

sig_atomic_t lock;
void sighandler(int, siginfo_t*, void*);

int main(int argc, const char* argv[]) {
  
  int ret;
  pid_t waitpid;
  struct sigaction newact;
  pid_t child;
  int i;
  int status;

  child = fork();
  if (child < 0) {
    perror("Can't create fork:");
    return 1;
  } else if (child == 0) {
    printf("Child\n");
    lock = 1;

    ret = sigemptyset(&(newact.sa_mask));
    if (ret < 0) {
      perror("Error empty mask set:");
      return 1;
    }

    newact.sa_flags = SA_SIGINFO;
    newact.sa_handler = NULL;
    newact.sa_sigaction = &sighandler;

    sigaction(SIGUSR1, &newact, NULL);

    sigset_t newset;
    sigset_t oldset;
    sigemptyset(&newset);
    sigemptyset(&oldset);
    sigaddset(&newset, SIGUSR1); //block SIGUSR1
    sigprocmask(SIG_BLOCK, &newset, &oldset);
    sleep(10); // critical segments
    while (lock) {
      printf("Now lock is %d\n", lock);
      sigsuspend(&oldset);
      printf("Child: I received SIGUSR1\n");
    }
    printf("Child finished\n");

    return 110;
  } else {
    printf("Parent\n");
    sleep(3);
    kill(child, SIGUSR1);
    printf("Parent: I sent SIGUSR1 to my son\n");
    waitpid = wait(&status);
    if (waitpid < 0) {
      perror("Error wait:");
      return 1;
    }
    printf("Parent finished.\n");
  }

  return 0;
}

void sighandler(int sig, siginfo_t* info, void* context)
{
  lock = 0;
  printf("Now lock is %d\n", lock);
}
