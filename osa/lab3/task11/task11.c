#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/wait.h>

sig_atomic_t lock;

void sighandler (int sig, siginfo_t *info, void* context) {
  
  int i;

  printf ("Standart handler\n");
  lock = 0;

}

int main(int argc, const char* argv[]) {
  
  int ret;
  pid_t waitpid;
  struct sigaction newact;
  pid_t child;
  int i;
  int status;

  /*
  sigemptyset(&(newact.sa_mask));
  newact.sa_flags = 0;
  newact.sa_sigaction = NULL;
  newact.sa_handler = SIG_IGN;

  sigaction(SIGCHLD, &newact, NULL);
  */

  child = fork();
  if (child < 0) {
    perror("Can't create fork:");
    return 1;
  } else if (child == 0) {
    printf("Child\n");
    lock = 1;

    ret = sigemptyset(&(newact.sa_mask));
    if (ret < 0) {
      perror("Error empty mask set:");
      return 1;
    }

    newact.sa_flags = SA_SIGINFO;
    newact.sa_handler = NULL;
    newact.sa_sigaction = &sighandler;

    sigaction(SIGUSR1, &newact, NULL);

    while (lock) {
      printf("Locked\n");
    }

    return 110;
  } else {
    printf("Parent\n");
    for (i = 0; i < 10000; ++i) {
      printf("Parent:%i\n", i);
    }
    kill(child, SIGUSR1);
    waitpid = wait(&status);
    if (waitpid < 0) {
      perror("Error wait:");
      return 1;
    }
    printf("%i\n", WEXITSTATUS(status));
    printf("%i\n", WIFSIGNALED(status));
    printf("%i\n", WTERMSIG(status));
    printf("%X\n", status);
  }

  return 0;
}
