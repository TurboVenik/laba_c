#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (int argc, char* argv[]) {
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("Child with PID %d starts\n", getpid());
		pause();
		printf("Child has finished\n");
		exit(0);
	}
	else {
		printf("Parent pid is %d\n", getpid());
		int status;
		int pid;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}
		printf("Child with PID %d returned status code %d\n", pid, WEXITSTATUS(status));
		if (WIFSIGNALED(status)){
			printf("Child: I recieved signal no. %d\n", WTERMSIG(status));
		}
		else
			printf("Child: I didn't recieve any signals\n");	
			printf("Parent is gone\n");
	}
	return 0;
}