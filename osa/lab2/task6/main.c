#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\n\tChild PID after fork is %d\n", getpid());
		printf("\tChild PPID after fork is %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("\tChild PGID after fork is %d\n", pgid);

		pause();
		printf("\n\tChild: It's time to go to better place...\n\n");
		exit(0);

	}
	else{
		printf("Parent PID after fork is %d\n", getpid());
		printf("Parent PPID after fork is %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("Parent PGID after fork is %d\n", pgid);

		pause();
		printf("\nFather: It's time to go to better place...\n\n");
		exit(0);
	}

	return 0;
}