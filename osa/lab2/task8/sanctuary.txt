Snow is falling quietly
Careless of it's destiny
Emptyness will kill me soon
Tonight they'll dance outside my bedroom
Someone's walking happily
Carry all their mystery
Isolation's killing me
Tonight they'll dance for me
Wind is whistling down the street
Drowning sounds of happy feet
Temptation's killing me
Tonight the wind will sing for me
Colors fade into the night
They're refugees in neon lights
Darkness is killing me
Later light will shine on me
You'll be standing next to me
Soon you will be kissing me
Happiness is killing me
Tonight you will be dancing for me
