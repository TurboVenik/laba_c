#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return 1;
	}
	if (pid == 0){
		printf("Luke: Nooooooooo\n");
	}
	else{
		printf("Batya: Luke, I Am Your Father\n");
	}

	return 0;
}