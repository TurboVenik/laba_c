#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		sleep(3);
		printf("\tChild PID is %d\n", getpid());
		printf("\tChild PPID is %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("\tChild PGID is %d\n", pgid);

		exit(7);

	}
	else{
		printf("Parent PID is %d\n", getpid());
		printf("Parent PPID is %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("Parent PGID is %d\n", pgid);

		int status;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}

		printf("Child with PID %d returned status code %d\n", pid, WEXITSTATUS(status));
	}

	return 0;
}