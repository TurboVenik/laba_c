#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\tChild PID after fork is %d\n", getpid());
		printf("\tChild PPID after fork is %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("\tChild PGID after fork is %d\n", pgid);

		printf("\n\tNow we are waiting...\n");
		fflush(stdout);
		sleep(3);
		printf("\tMaybe father is already dead...\n");
		fflush(stdout);
		sleep(3);
		printf("\tMy turn will come soon...\n\n");
		fflush(stdout);
		sleep(3);

		printf("\tChild PID in the face of death is %d\n", getpid());
		printf("\tChild PPID in the face of death is %d\n", getppid());
		pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("\tChild PGID in the face of death is %d\n", pgid);

		printf("\n\tIt's time to go to better place...\n");
		exit(0);

	}
	else{
		printf("Parent PID after fork is %d\n", getpid());
		printf("Parent PPID after fork is %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("Parent PGID after fork is %d\n", pgid);

		printf("By son, I love you so much...\n\n");
		exit(0);
	}

	return 0;
}