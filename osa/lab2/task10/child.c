#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

int main(int argc, char* argv[], char* envp[])
{
	printf(".....::::: Child process :::::.....\n");
	printf("\nMy argument list:\n");
	for (int i = 0; i < argc; ++i){
		printf("Arg no. %d is \"%s\"\n", i, argv[i]);
	}

	printf("\nMy environment variables list:\n");
	for (int i = 0; envp[i] != 0; ++i){
		printf("Env. variable no. %d is \"%s\"\n", i, envp[i]);
	}

	return 0;
}