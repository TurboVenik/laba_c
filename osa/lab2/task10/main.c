#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

void get_name_from_path(char *, char *);

int main(int argc, char* argv[], char* envp[])
{
	if (argc != 2){
		printf("Program takes exactly 1 argument, but %d was/were transferred.\n", argc - 1);
		return -1;
	}

	char *path = argv[1];
	char name[256];
	get_name_from_path(path, name);

	int pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}

	// Child
	if (pid == 0){
		int ret = execlp(path, name, NULL);
		if (ret == -1){
			perror("Error: ");
			return -1;
		}
	}
	else{ // Parent
		int status;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}

		printf("\n.....:::: Parent process :::::.....\n");
		printf("\nMy arguments list:\n");
		for (int i = 0; i < argc; ++i){
			printf("Arg no. %d is \"%s\"\n", i, argv[i]);
		}

		printf("\nMy environment variables list:\n");
		for (int i = 0; envp[i] != 0; ++i){
			printf("Env. variable no. %d is \"%s\"\n", i, envp[i]);
		}
	}

	return 0;
}

void get_name_from_path(char *path, char *name)
{
	char *tmp;
	int index;

	tmp = strrchr(path, '/');
	if (tmp == NULL){
		strcpy(name, path);
		return;
	}
	else{
		int i = 0;
		index = (int)(tmp - path);
		while (path[index] != '\0') {
			++index;
			name[i] = path[index];
			++i;
		}
		return;
	}
}