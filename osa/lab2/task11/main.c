#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

void get_name_from_path(char *, char *);

int main(int argc, char* argv[], char* envp[])
{
	if (argc == 1){
		printf("There are no arguments here.\n");
		return -1;
	}
	
	char *path = argv[1];
	char name[256];
	get_name_from_path(path, name);

	char *child_argv[argc - 1];
	child_argv[0] = strdup(name);
	for (int i = 1; i < argc - 1; ++i){
		child_argv[i] = strdup(argv[i + 1]);
	}
	child_argv[argc - 1] = NULL;

	int pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}

	// Child
	if (pid == 0){
		int ret = execvp(path, child_argv);
		if (ret == -1){
			perror("Error: ");
			return -1;
		}
	}
	else{ // Parent
		int status;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}
	}

	return 0;
}

void get_name_from_path(char *path, char *name)
{
	char *tmp;
	int index;

	tmp = strrchr(path, '/');
	if (tmp == NULL){
		strcpy(name, path);
		return;
	}
	else{
		int i = 0;
		index = (int)(tmp - path);
		while (path[index] != '\0') {
			++index;
			name[i] = path[index];
			++i;
		}
		return;
	}
}