#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

int main (int argc, char* argv[]) {
    int msqid = atoi(argv[1]);
    int mtype = atoi(argv[2]);

    struct msqid_ds ds;
    int res = msgctl(msqid, IPC_STAT, &ds);
    if (res == -1) {
        perror("Msgctl: ");
        return 1;
    }

    typedef struct my_msgbuf {
        long mtype;
        char payload[ds.msg_qbytes]; 
    } my_msgbuf;

    struct my_msgbuf msg;
    int length = sizeof(my_msgbuf) - sizeof(long); 
    res = msgrcv(msqid, &msg, length, mtype, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    printf("Message 1: %s\n", msg.payload);
}
