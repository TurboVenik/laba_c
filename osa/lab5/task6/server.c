#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

typedef struct my_msgbuf {
    long mtype;
    int msg_qid;
    int payload; 
} my_msgbuf;

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    int key = ftok("./server", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int server_qid = msgget(key, 0666 | IPC_CREAT);
    if (server_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    printf("Server has started\n");

    struct my_msgbuf r_msg;
    struct my_msgbuf s_msg;
    int client_qid;
    int payload;

    while(1){
        read_message(server_qid, 0, &r_msg);
        client_qid = r_msg.msg_qid;
        payload = r_msg.payload;
        printf("I get message with payload %d\n", payload);
        s_msg.mtype = 1;
        s_msg.msg_qid = 0;
        s_msg.payload = payload + 1;
        send_message(client_qid, &s_msg);
    }
}
