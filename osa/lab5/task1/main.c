#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

#define MY_DATA 8
typedef struct my_msgbuf {
    long mtype;
    char payload[MY_DATA]; 
} my_msgbuf;

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
}

int main (int argc, char* argv[]) {
    int key = ftok("./main", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int msqid = msgget(key, 0666 | IPC_CREAT);
    if (msqid == -1) {
        perror("Msgget: ");
        return 1;
    }

    struct msqid_ds ds;

    int res = msgctl(msqid, IPC_STAT, &ds);
    if (res == -1) {
        perror("Msgctl: ");
        return 1;
    }

    printf("Owner's UID: %d\n", ds.msg_perm.uid);
    printf("Owner's GID: %d\n", ds.msg_perm.gid);
    printf("Message stime: %d\n", ds.msg_stime);
    printf("Message rtime: %d\n", ds.msg_rtime);
    printf("Message ctime: %d\n", ds.msg_ctime);
    printf("Number of messages: %d\n", ds.msg_qnum);
    printf("Maximum number of bytes: %d\n", ds.msg_qbytes);

    struct my_msgbuf msg1;
    msg1.mtype = 1;
    strcpy(msg1.payload, "Hi!");
    send_message(msqid, &msg1);

    struct my_msgbuf msg2;
    msg2.mtype = 2;
    strcpy(msg2.payload, "Hi!");
    send_message(msqid, &msg2);

    struct my_msgbuf msg3;
    msg3.mtype = 3;
    strcpy(msg3.payload, "Hi!");
    send_message(msqid, &msg3);
}
