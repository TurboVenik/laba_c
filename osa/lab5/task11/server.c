#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

typedef struct my_msgbuf {
    long mtype; 
} my_msgbuf;

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, IPC_NOWAIT);
    if (res == -1) {
        if (errno == ENOMSG){
            return 0;
        }
        perror("Msgrcv: ");
        return -1;
    }
    return 1;
}

int main (int argc, char* argv[]) {
    int key = ftok("./server", 'q');    // questions
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int server_qid = msgget(key, 0666 | IPC_CREAT);
    if (server_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    printf("Server has started\n");

    struct my_msgbuf r_msg;
    int res;
    int count = 0;

    while(1){
        res = read_message(server_qid, 1, &r_msg);
        if (res == 0){
            printf("Now I am working... Step %d\n", count);
            ++count;
            sleep(1);
        }
        else{
            while(1){
                res = read_message(server_qid, 1, &r_msg);
                if (res == 0){
                    sleep(1);
                }
                else{
                    break;
                }
            }
        }
    }
}
