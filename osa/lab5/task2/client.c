#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

#define MY_DATA 8
typedef struct my_msgbuf {
    long mtype;
    char payload[MY_DATA]; 
} my_msgbuf;

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
}

int main (int argc, char* argv[]) {
    int msqid = atoi(argv[1]);
    int mtype = atoi(argv[2]);

    struct my_msgbuf msg1;
    read_message(msqid, mtype, &msg1);
    printf("Message 1: %s\n", msg1.payload);
    struct my_msgbuf msg2;
    read_message(msqid, mtype, &msg2);
    printf("Message 2: %s\n", msg2.payload);
    struct my_msgbuf msg3;
    read_message(msqid, mtype, &msg3);
    printf("Message 3: %s\n", msg3.payload);
}
