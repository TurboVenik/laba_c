#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
	int fd = 0;
	char filename [256];
	char mode_buf [5];

	printf("Enter filename: ");
	fgets(filename, 256, stdin);
	filename[strlen(filename) - 1] = 0;
	printf("Enter mode: ");
	fgets(mode_buf, 5, stdin);

	char* end;
	long mode = strtol(mode_buf, &end, 8);

	printf("Filename is %s\n", filename);
	printf("Mode is %d\n", mode);

	printf("I am trying to create file in read-only mode.\n");
	fd = open(filename, (O_WRONLY | O_CREAT | O_TRUNC), mode);
	if(fd < 0)
		printf("\tError: %s\n", strerror(errno));
	else{
		printf("\tOpen --- OK. File descriptor is %d\n", fd);
		char* text = "\t\tString 1\n\t\tString 2\n\t\tString 3\n\t\tString 4\n\t\tString 5\n";
		int count = write(fd, text, strlen(text));
		if(count < 0){
			printf("\tError: %s\n", strerror(errno));
		}
		else{
			printf("\tWrite --- OK. %d bytes was wrote.\n", count);
		}
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
		printf("\tClose --- OK. File descriptor no.%d is closed.\n", fd);
	}

	printf("I am trying to open file in O_RDRW mode.\n");
	fd = open(filename, (O_RDWR));
	if(fd < 0)
		printf("\tError: %s\n", strerror(errno));
	else{
		printf("\tOpen --- OK. File descriptor is %d\n", fd);

		int off = lseek(fd, 1000000, SEEK_END);
		if(off < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tLseek --- OK. SEEK_END  + 1000000 mode. Offset is %d\n", off);

		char* text = "\t\tString 6\n\t\tString 7\n\t\tString 8\n\t\tString 9\n\t\tString 10\n";
		int count = write(fd, text, strlen(text));
		if(count < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tWrite --- OK. %d bytes was wrote.\n", count);

		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}

	return 0;
}
