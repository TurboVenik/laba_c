#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	if(argc != 2){
		printf("Program takes exactly 1 argument - filename\n");
		exit(1);
	}

	char* filename = argv[1];
	int fd = 0;
	printf("Filename is %s\n\n", filename);

	printf("I am trying to open file in read-only mode.\n");
	fd = open(filename, (O_RDONLY), 0644);
	if(fd < 0){
		printf("\tError code: %d\n", errno);
		printf("\tError text from strerror(): %s\n", strerror(errno));
		perror("");
	}
	else{
		printf("\tEverything is ok. File descriptor is %d\n", fd);
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}
	printf("\n");

	printf("I am trying to open file in write-only mode.\n");
	fd = open(filename, (O_WRONLY), 0644);
	if(fd < 0){
		printf("\tError code: %d\n", errno);
		printf("\tError text from strerror(): %s\n", strerror(errno));
		perror("\tError text from perror(): ");
	}
	else{
		printf("\tEverything is ok. File descriptor is %d\n", fd);
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}
	printf("\n");


	printf("I am trying to open file in read-write mode.\n");
	fd = open(filename, (O_RDWR), 0644);
	if(fd < 0){
		printf("\tError code: %d\n", errno);
		printf("\tError text from strerror(): %s\n", strerror(errno));
		perror("\tError text from perror(): ");
	}
	else{
		printf("\tEverything is ok. File descriptor is %d\n", fd);
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}
	printf("\n");

	printf("I am trying to open file in O_CREAT and O_EXCL mode.\n");
	fd = open(filename, (O_CREAT | O_EXCL), 0644);
	if(fd < 0){
		printf("\tError code: %d\n", errno);
		printf("\tError text from strerror(): %s\n", strerror(errno));
		perror("\tError text from perror()");
	}
	else{
		printf("\tEverything is ok. File descriptor is %d\n", fd);
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}
	printf("\n");

	printf("I am trying to open file in O_CREAT mode.\n");
	fd = open(filename, (O_CREAT), 0644);
	if(fd < 0){
		printf("\tError code: %d\n", errno);
		printf("\tError text from strerror(): %s\n", strerror(errno));
		perror("\tError text from perror()");
	}
	else{
		printf("\tEverything is ok. File descriptor is %d\n", fd);
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}
	printf("\n");

	return 0;
}
