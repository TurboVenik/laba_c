#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
    if (argc < 3){
        printf("Program takes 2 or more arguments, but %d was/were transferred.\n", argc - 1);
        return 1;
    }

    int max_size = 0;
    int max_file_pos = 0;

    for (int i = 1; i < argc; ++i){
        struct stat file_stat;
        if(stat(argv[i], &file_stat) < 0){
            printf("Error: %s\n", strerror(errno));
            return 2;
        }
        if (file_stat.st_size > max_size){
            max_size = file_stat.st_size;
            max_file_pos = i;
        }
    }

    printf("The biggest file is \"%s\"\n", argv[max_file_pos]);
    printf("Size is %d bytes.\n", max_size);

    return 0;
}
