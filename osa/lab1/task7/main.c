#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
	if (argc != 2){
		printf("Program takes exactly 1 argument, but %d was/were transferred.\n", argc - 1);
		return 1;
	}

	struct stat file_stat;
	if(stat(argv[1], &file_stat) < 0){
		printf("Error: %s\n", strerror(errno));
		return 2;
	}
 
    printf("Information for %s\n", argv[1]);
    printf("------------------------------------------------\n");
    printf("File Size: \t\t%d bytes\n", file_stat.st_size);
    printf("Number of Links: \t%d\n", file_stat.st_nlink);
    printf("File inode: \t\t%d\n", file_stat.st_ino);
    printf("User ID of owner: \t%d\n", file_stat.st_uid);
    printf("Groyp ID of owner: \t%d\n", file_stat.st_gid);
    printf("Device ID: \t\t%d\n", file_stat.st_rdev);
 
    printf("File Permissions: \t");
    printf( (S_ISDIR(file_stat.st_mode)) ? "d" : "-");
    printf( (file_stat.st_mode & S_IRUSR) ? "r" : "-");
    printf( (file_stat.st_mode & S_IWUSR) ? "w" : "-");
    printf( (file_stat.st_mode & S_IXUSR) ? "x" : "-");
    printf( (file_stat.st_mode & S_IRGRP) ? "r" : "-");
    printf( (file_stat.st_mode & S_IWGRP) ? "w" : "-");
    printf( (file_stat.st_mode & S_IXGRP) ? "x" : "-");
    printf( (file_stat.st_mode & S_IROTH) ? "r" : "-");
    printf( (file_stat.st_mode & S_IWOTH) ? "w" : "-");
    printf( (file_stat.st_mode & S_IXOTH) ? "x" : "-");
    printf("\n\n");
 
    printf("The file %s a symbolic link\n", (S_ISLNK(file_stat.st_mode)) ? "is" : "is not");

	return 0;
}
