#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
    if (argc != 2){
        printf("Program takes exactly 1 argument, but %d was/were transferred.\n", argc - 1);
        return 1;
    }

    int fd = open(argv[1], (O_RDONLY));
    if(fd < 0)
        printf("Error: %s\n", strerror(errno));
    else{
        //get size of file
        int fsize = lseek(fd, 0, SEEK_END);
        if(fsize < 0){
            printf("Error: %s\n", strerror(errno));
            return 2;
        }

        char microbuffer;
        for (int i = fsize - 2; i >= 0; --i){
            //set position
            int off = lseek(fd, i, SEEK_SET);
            if(fsize < 0){
                printf("Error: %s\n", strerror(errno));
                return 2;
            }
            //read 1 byte
            int count = read(fd, &microbuffer, 1);
            if (count < 0)
                printf("Error: %s\n", strerror(errno));
            else{
                //write 1 byte to stdout
                count = write(1, &microbuffer, 1);
                if (count < 0){
                    printf("\tError: %s\n", strerror(errno));
                    return 3;
                }
            }

        }

        printf("\n");

        if (close(fd) < 0){
            printf("Error: %s\n", strerror(errno));
            return 3;
        }
    }
}
