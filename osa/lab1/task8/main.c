#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int copy(int, int);

int main(int argc, char* argv[])
{
    int from_fd;
    int to_fd;

	if (argc == 1){
        printf("Copy stdin to stdout.\n");
        from_fd = 0;
        to_fd = 1;
	}
    else if (argc == 3) {
        printf("Copy from \"%s\" to \"%s\"\n", argv[1], argv[2]);
        from_fd = open(argv[1], (O_RDONLY));
        if(from_fd < 0){
            printf("\tError: %s\n", strerror(errno));
            return 1;
        }
        to_fd = open(argv[2], (O_WRONLY | O_CREAT | O_TRUNC), 0666);
        if(to_fd < 0){
            printf("\tError: %s\n", strerror(errno));
            return 1;
        }
    }
    else {
        printf("Wrong number of arguments.\n");
        return 2;
    }

    int result = copy(from_fd, to_fd);
    if (result < 0){
        printf("Copying wasn't successful.\n");
        return 3;
    }
    else
        printf("Copying finished successfully.\n");

    if (close(from_fd) < 0){
            printf("\tError: %s\n", strerror(errno));
            return 4;
    }

    if (close(to_fd) < 0){
            printf("\tError: %s\n", strerror(errno));
            return 4;
    }

	return 0;
}

int copy(int from_fd, int to_fd)
{
    char* buffer[1000];
    int r_count = 0;
    int w_count = 0;

    do {
        r_count = read(from_fd, buffer, 1000);
        if (r_count < 0){
                printf("\tError: %s\n", strerror(errno));
                return -1;
        }
        w_count = write(to_fd, buffer, r_count);
        if (w_count < 0){
                printf("\tError: %s\n", strerror(errno));
                return -1;
        }

    } while(r_count != 0);

    return 0;
} 
