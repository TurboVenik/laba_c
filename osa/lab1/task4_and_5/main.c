#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
	int fd = 0;
	char filename [256];
	char mode_buf [5];

	printf("Enter filename: ");
	fgets(filename, 256, stdin);
	filename[strlen(filename) - 1] = 0;
	printf("Enter mode: ");
	fgets(mode_buf, 5, stdin);

	char* end;
	long mode = strtol(mode_buf, &end, 8);

	printf("Filename is %s\n", filename);
	printf("Mode is %d\n", mode);

	printf("I am trying to create file in read-only mode.\n");
	fd = open(filename, (O_WRONLY | O_CREAT | O_TRUNC), mode);
	if(fd < 0)
		printf("\tError: %s\n", strerror(errno));
	else{
		printf("\tOpen --- OK. File descriptor is %d\n", fd);
		char* text = "\t\tString 1\n\t\tString 2\n\t\tString 3\n\t\tString 4\n\t\tString 5\n";
		int count = write(fd, text, strlen(text));
		if(count < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tWrite --- OK. %d bytes was wrote.\n", count);
		printf("\tClose --- OK. File descriptor no.%d is closed.\n", fd);
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}

	printf("I am trying to open file in O_RDRW mode.\n");
	fd = open(filename, (O_RDWR));
	if(fd < 0)
		printf("\tError: %s\n", strerror(errno));
	else{
		printf("\tOpen --- OK. File descriptor is %d\n", fd);
		char* buffer[200];

		int off = lseek(fd, 11, SEEK_SET);
		if(off < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tLseek --- OK. SEEK_SET mode. Offset is %d\n", off);

		int count = read(fd, buffer, 11);
		if(count < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tRead --- OK. Information from file is:\n%s\t\t%d bytes was read.\n", buffer, count);

		off = lseek(fd, 11, SEEK_CUR);
		if(off < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tLseek --- OK. SEEK_CUR + 11 mode. Offset is %d\n", off);

		count = read(fd, buffer, 11);
		if(count < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tRead --- OK. Information from file is:\n%s\t\t%d bytes was read.\n", buffer, count);

		off = lseek(fd, -11, SEEK_END);
		if(off < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tLseek --- OK. SEEK_END - 11 mode. Offset is %d\n", off);

		count = read(fd, buffer, 11);
		if(count < 0)
			printf("\tError: %s\n", strerror(errno));
		else
			printf("\tRead --- OK. Information from file is:\n%s\t\t%d bytes was read.\n", buffer, count);
		printf("\tClose --- OK. File descriptor no.%d is closed.\n", fd);
		if (close(fd) < 0){
			printf("\tError: %s\n", strerror(errno));
			return 1;
		}
	}

	return 0;
}
