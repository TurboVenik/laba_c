#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char* argv[]) {
	int pid;
	pid = fork();
	if (pid == -1) {
		perror("Error: ");
		return 1;
	}
	if (pid == 0) {
		time_t rawtime1 = time(0);
	  printf ( "son = %d \n", rawtime1 );
	}
	else {
		time_t rawtime2 = time(0);
	  printf ( "Otec = %d \n", rawtime2 );
	}

	return 0;
}
