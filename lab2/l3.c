#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char* argv[], char* env[]) {
	int pid;

	sleep(10);
	printf("hi");

	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		// sleep(1);
		printf("\tChild PID: %d\n", getpid());
		printf("\tChild PPID: %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}

		printf("\tChild PGID: %d\n", pgid);
		pause();
	}
	else {
		printf("Parent PID: %d\n", getpid());
		printf("Parent PPID: %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}
		else
			printf("Parent PGID: %d\n", pgid);

		int status;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}

		printf("Child PID %d returned: %d %d\n", pid, WEXITSTATUS(status), status);
	}

	return 0;
}
