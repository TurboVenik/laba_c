#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

extern char **environ;

int main(int argc, char* argv[])
{
	if (argc < 2){
		printf("I want at least one parametr.\n");
		return -1;
	}

	printf("\nMy environment variables list before putenv():\n");
	char *s = *environ;
	for (int i = 1; s != NULL; ++i){
		printf("env[%d]%s\n", i, s);
		s = *(environ + i);
	}

	for (int i = 1; i < argc; ++i)
	{
		int ret = putenv(argv[i]);
		if (ret < 0){
			perror("Error: ");
			return -1;
		}
	}

	printf("\nMy environment variables list after putenv():\n");
	s = *environ;
	for (int i = 1; s != NULL; ++i){
		printf("env[%d]%s\n", i, s);
		s = *(environ + i);
	}

	return 0;
}
