#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
	int pid;
	pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}
	if (pid == 0){
		printf("\tChild PID after fork: %d\n", getpid());
		printf("\tChild PPID after fork: %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}

		printf("\tChild PGID after fork: %d\n", pgid);

		printf("Waiting ...");
		fflush(stdout);

		sleep(3);

		printf("\tChild PID after waiting: %d\n", getpid());
		printf("\tChild PPID after waiting: %d\n", getppid());
		pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}

		printf("\tChild PGID after waiting: %d\n", pgid);

		exit(0);
	}
	else {
		printf("Parent PID after fork: %d\n", getpid());
		printf("Parent PPID after fork: %d\n", getppid());
		int pgid = getpgid(0);
		if (pgid == -1){
			perror("Error: ");
			return -1;
		}

		printf("Parent PGID after fork: %d\n", pgid);

		printf("Exit...\n\n");
		exit(0);
	}

	return 0;
}
