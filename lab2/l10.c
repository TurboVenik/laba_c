#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

int main(int argc, char* argv[], char* envp[]) {
	if (argc != 2){
		printf("Program takes exactly 1 argument, but %d was/were transferred.\n", argc - 1);
		return -1;
	}
	int ret = putenv("cbb=ddd");
	if (ret == -1){
		perror("Error: ");
		return -1;
	}

	int pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}

	// Child

	if (pid == 0){
		// int ret = putenv("aa=bbb");
		if (ret == -1){
			perror("Error: ");
			return -1;
		}
		ret = execlp(argv[1], "hello", "world", NULL);
		if (ret == -1){
			perror("Error: ");
			return -1;
		}
	}
	else{ // Parent
		printf("\n1!!!!!!!!!!!!!!!!!!!! Parent process ========\n");


		printf("\nEnvironment variables:\n");
		for (int i = 0; envp[i] != 0; ++i){
			printf("envp[%d] is \"%s\"\n", i, envp[i]);
		}
		int status;
		pid = wait(&status);
		if (pid == -1){
			perror("Error: ");
			return -1;
		}
		printf("\n2!!!!!!!!!!!!!!!!!!! Parent process ========\n");


		printf("\nEnvironment variables:\n");
		for (int i = 0; envp[i] != 0; ++i){
			printf("envp[%d] is \"%s\"\n", i, envp[i]);
		}

	}

	return 0;
}
