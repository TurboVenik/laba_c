#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

int copy(int, int);

int main(int argc, char* argv[]) {
	if (argc != 2){
		printf("Must be one arg.\n");
		return -1;
	}

	int source_fd = open(argv[1], O_RDONLY);
	if(source_fd < 0) {
		perror("Error: ");
		return -1;
	}

	int pid = fork();
	if (pid == -1){
		perror("Error: ");
		return -1;
	}

	if (pid == 0){
		copy(source_fd, 1);
		exit(0);
	}
	else {
		copy(source_fd, 1);
	}
	return 0;
}

int copy(int source_fd, int fd)
{
    char* buffer[1];
    int r_count = 0;
    int w_count = 0;

    do {
        r_count = read(source_fd, buffer, 1);
        if (r_count < 0){
                printf("\tError: %s\n", strerror(errno));
                return -1;
        }
        w_count = write(fd, buffer, r_count);
        if (w_count < 0){
                printf("\tError: %s\n", strerror(errno));
                return -1;
        }

    } while(r_count != 0);

    return 0;
}
